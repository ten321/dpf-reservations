=== Fair Reservations ===
Contributors: cgrymala
Requires at least: 4.6
Tested up to: 4.9
Stable tag: 2.0.6
License: GPLv2 or later

This plugin implements various functionality related to running the fairs distributed by Inner Prizes.

== Description ==
This plugin adds some automated functionality allowing vendors and practitioners to set up their profiles, purchase their spaces and set up their availability for a fair presented/distributed by Inner Prizes.

== Installation ==
1. Upload the dpf-reservations folder to wp-content/plugins
1. Activate the plugin
1. Edit your `wp-config.php` file:
    1. Define the `FAIR_WEEK_NUMBER` constant to indicate which week of the month the fair occurs (e.g. if the fair is on the 2nd Sunday of each month, use `define( 'FAIR_WEEK_NUMBER', 2 )` )
    1. If you will be using something other than "Reader(s)" and "Vendor(s)" as the names of your user types, define the `DPF_READER_NAME_S`, `DPF_READER_NAME_P`, `DPF_VENDOR_NAME_S` and `DPF_VENDOR_NAME_P` constants to define Reader and Vendor singular and plural labels
    1. If you will not be offering workshops and classes, you can define the `FAIR_NO_WORKSHOPS` constant and set it to `true` to avoid printing workshop setup instructions

== Changelog ==
= 2.0.6 =
* Rewrite instructions for fair setup
* Split instructions for fair setup into separate functions to make them easier to find/modify
* Implement multiple possibilities for vendor registration structure
* Implement option to exclude Classes & Workshops instructions

= 2.0 =
Major rewrite. Update the styles of various items to be more modern and flexible

= 1.0 =
First distributed version