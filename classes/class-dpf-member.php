<?php
/**
 * A class to handle individual readers
 */
class DPF_Member {
	var $id=null;
	var $firstname = null;
	var $lastname = null;
	var $photo = null;
	var $focus = null;
	var $bio = null;
	var $email = null;
	var $times = array();
	private static $instance;
	
	/**
	 * Returns the instance of this class.
	 *
	 * @access  public
	 * @since   2.0
	 * @return  DPF_Member
	 */
	public static function instance() {
		if ( ! isset( self::$instance ) ) {
			$className      = __CLASS__;
			self::$instance = new $className;
		}
		
		return self::$instance;
	}
	
	/**
	 * Construct our DPF_Member object
	 */
	function __construct( $props=array() ) {
		foreach ( $props as $k => $v ) {
			switch( $k ) {
				case 'm_id' :
					$this->id = intval( $v );
					break;
				case 'm_first' :
					$this->firstname = ucfirst( strtolower( $v ) );
					break;
				case 'm_last' :
					$this->lastname = ucfirst( strtolower( $v ) );
					break;
				case 'm_picture' :
					$this->photo = ( isset( $_SERVER['HTTPS'] ) && 'on' == $_SERVER['HTTPS'] ? 'https' : 'http' ) . '://www.dallaspsychicfair.com/bio_pics/' . $v;
					$this->photofilename = $v;
					break;
				case 'm_focus' :
					$this->focus = $v;
					break;
				case 'm_desc' :
					$this->bio = $v;
					break;
				case 'm_email' :
					$this->email = is_email( $v );
					break;
				case 'm_pass' :
					$this->password = $v;
					break;
				case 'm_user' :
					$this->username = $v;
					break;
				case 'm_web' :
					$this->url = esc_url( $v );
					break;
				case 'm_type' :
					$this->type = $v;
					break;
			}
		}
		
		/*$this->convert_user();*/
	}
	
	/**
	 * Output the reader's brief bio information
	 */
	function summary() {
		echo $this->get_summary();
	}
	
	/**
	 * Build the reader's brief bio information
	 */
	function get_summary() {
		global $wpdb;
		$user = get_user_by( 'email', $this->email );
		$reader = get_posts( array( 'author' => $user->ID, 'post_type' => 'reader', 'post_status' => 'publish' ) );
		if ( ! is_array( $reader ) || empty( $reader ) || is_wp_error( $reader ) )
			return false;
		
		$reader = array_shift( $reader );
		
		$this->id = $reader->ID;
		$this->firstname = get_user_meta( $user->ID, 'first_name', true );
		$this->lastname = get_user_meta( $user->ID, 'last_name', true );
		$this->photo = has_post_thumbnail( $reader->ID ) ? get_the_post_thumbnail( $reader->ID, 'reader-small', array( 'class' => 'reader-photo' ) ) : get_avatar( $user->ID, 100, null, $this->firstname . ' ' . $this->lastname );
		$this->focus = $reader->post_excerpt;
		$this->bio = get_the_content( $reader->ID );
		$this->email = $reader->email;
		
		$rt = '<article class="reader-summary" id="reader-' . $reader->ID . '">';
		$rt .= $this->photo;
		$rt .= '<h2 class="reader-name"><a href="#reader-bio-' . $reader->ID . '" class="nyroModal">' . $this->firstname . ' ' . $this->lastname . '</a></h2>';
		$rt .= '<p class="reader-focus">' . $this->focus . '</p>';
		$rt .= '<article id="reader-bio-' . $reader->ID . '" class="hide-if-js">' . apply_filters( 'the_content', $reader->post_content ) . '</article>';
		$rt .= '</article>';
		
		return $rt;
	}
	
	/**
	 * Output the reader's full bio
	 */
	function content() {
		echo $this->get_content();
	}
	
	/**
	 * Build the reader's full bio
	 */
	function get_content() {
		$rt = '<article class="reader-bio" id="reader-' . $this->id . '">';
		$rt .= '<img src="' . $this->photo . '" style="width: 100%; max-width: 100%; height: auto;"/>';
		$rt .= '<h2 class="reader-name">' . $this->firstname . ' ' . $this->lastname . '</h2>';
		$rt .= '<p class="reader-focus">' . $this->focus . '</p>';
		$rt .= '<section class="reader-bio-text">' . $this->bio . '</section>';
		$rt .= '</article>';
		
		return $rt;
	}
	
	function convert_user() {
		$uid = null;
		if ( ! empty( $this->email ) )
			$uid = get_user_by( 'email', $this->email );
		
		/*if ( ! empty( $uid ) )
			return error_log( 'The user with an email address of ' . $this->email . ' already exists in WordPress' );*/
		
		$user = array(
			'user_pass'    => $this->password,
			'user_login'   => $this->username,
			'user_url'     => $this->url,
			'user_email'   => $this->email,
			'display_name' => $this->firstname . ' ' . $this->lastname,
			'first_name'   => $this->firstname,
			'last_name'    => $this->lastname,
			'description'  => null,
			'roles'        => array( 'author' ),
		);
		
		if ( ! empty( $uid ) ) {
			$user = clone $uid;
			$uid = $user->ID;
			$user->roles = array( 'author' );
			unset( $user->caps, $user->cap_key, $user->allcaps );
			$uid = wp_update_user( $user );
			if ( is_wp_error( $uid ) )
				die( $uid->get_error_message() );
		} else {
			$uid = wp_insert_user( $user );
			if ( is_wp_error( $uid ) )
				die( $uid->get_error_message() );
		}
		
		if ( empty( $uid ) || 'reader' != strtolower( $this->type ) )
			return error_log( 'The process of creating a WordPress user for ' . $this->email . ' failed for some reason' );
		/*die( 'The process of creating/updating a WordPress user for ' . $this->email . ' failed for some reason. The data we tried to send was: ' . print_r( $user, true ) );*/
		
		$postdata = array(
			'post_type' => 'reader',
			'comment_status' => 'closed',
			'ping_status' => 'closed',
			'post_author' => $uid,
			'post_content' => $this->bio,
			'post_excerpt' => $this->focus,
			'post_title' => ! empty( $this->lastname ) && ! empty( $this->firstname ) ? $this->lastname . ', ' . $this->firstname : $this->lastname . $this->firstname,
			'post_status' => 'publish',
		);
		
		global $wpdb;
		$post = $wpdb->get_var( $wpdb->prepare( "SELECT ID FROM {$wpdb->posts} WHERE post_type=%s AND post_title=%s LIMIT 1", 'reader', $postdata['post_title'] ) );
		
		if ( empty( $post ) ) {
			$post = wp_insert_post( $postdata, $error );
			if ( empty( $post ) )
				return error_log( 'There was an error inserting the post for the reader with an email of ' . $this->email );
			/*die( 'There was an error inserting the post for the reader with an email of ' . $this->email . '. The error returned was: ' . $error->get_error_message() );*/
		} else {
			if ( false !== ( $photoid = get_post_meta( $post, '_thumbnail_id', true ) ) )
				return;
		}
		
		if ( false !== ( $photoid = $this->get_user_photo( $post ) ) )
			return update_post_meta( $post, '_thumbnail_id', $photoid );
		
		/*die( 'There was an error importing the photo of ' . $this->email );*/
		error_log( 'There was an error importing the photo of ' . $this->email );
	}
	
	function get_user_photo( $post = null ) {
		if ( empty( $post ) )
			return;
		
		if( !class_exists( 'WP_Http' ) )
			include_once( ABSPATH . WPINC. '/class-http.php' );
		
		$photo = new WP_Http;
		$photo = $photo->request( $this->photo );
		if ( is_wp_error( $photo ) )
			die( $photo->get_error_message() );
		
		if( $photo['response']['code'] != 200 )
			return false;
		
		$attachment = wp_upload_bits( $this->photofilename, null, $photo['body'], date("Y-m", strtotime( $photo['headers']['last-modified'] ) ) );
		if( !empty( $attachment['error'] ) )
			return false;
		
		$filetype = wp_check_filetype( basename( $attachment['file'] ), null );
		
		$postinfo = array(
			'post_mime_type'	=> $filetype['type'],
			'post_title'		=> $this->firstname . ' ' . $this->lastname . ' photograph',
			'post_content'		=> '',
			'post_status'		=> 'inherit',
		);
		$filename = $attachment['file'];
		$attach_id = wp_insert_attachment( $postinfo, $filename, $post );
		
		if( ! function_exists( 'wp_generate_attachment_data' ) )
			require_once( ABSPATH . "wp-admin" . '/includes/image.php' );
		$attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
		wp_update_attachment_metadata( $attach_id,  $attach_data );
		return $attach_id;
	}
}
