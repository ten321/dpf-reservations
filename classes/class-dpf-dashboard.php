<?php
/**
 * Implements the various dashboard widgets & help information for the
 *      Dallas Psychic Fair model
 *
 * @package WordPress
 * @subpackage DPF Reservations
 * @version 2.0
 */
namespace {
	if ( ! defined( 'ABSPATH' ) ) {
		die( 'You do not have permission to access this file directly.' );
	}
}

namespace DPF {
	
	class Dashboard {
		/**
		 * @var \DPF\Dashboard $instance holds the single instance of this class
		 * @access private
		 */
		private static $instance;
		
		/**
		 * @var string $version holds the version number for the plugin
		 * @access public
		 */
		public $version = '2.0';
		
		/**
		 * @var array $reader_label the singular & plural names for "readers"
		 */
		public $reader_label = array();
		/**
		 * @var array $vendor_label the singular & plural names for "vendors"
		 */
		public $vendor_label = array();
		/**
		 * @var \DPF_Orders holds the instance of our main DPF_Orders object
		 */
		public $orders_obj;
		
		
		/**
		 * Creates the \DPF\Dashboard object
		 *
		 * @access private
		 * @since  2.0
		 */
		private function __construct() {
		    $current_user = wp_get_current_user();
		    if ( $current_user->exists() ) {
			    $userinfo = get_userdata( $current_user->ID );
			    if ( in_array( 'vendor', $current_user->roles ) || in_array( 'reader', $current_user->roles ) ) {
			        add_filter( 'woocommerce_disable_admin_bar', '__return_false' );
			        add_filter( 'woocommerce_admin_disabled', '__return_false' );
                }

            }

			add_action( 'wp_dashboard_setup', array( $this, 'manage_admin_dashboard' ), 1 );
			add_action( 'woocommerce_account_content', array( $this, 'dashboard_welcome_widget' ), 11 );

			add_filter( 'wcpv_remove_vendor_dashboard_widgets', '__return_false' );
			
			if ( defined( 'DPF_READER_NAME_S' ) && defined( 'DPF_READER_NAME_P' ) ) {
				$this->reader_label = array( 'singular' => DPF_READER_NAME_S, 'plural' => DPF_READER_NAME_P );
			} else {
				$this->reader_label = array( 'singular' => __( 'Reader' ), 'plural' => __( 'Readers' ) );
			}
			if ( defined( 'DPF_VENDOR_NAME_S' ) && defined( 'DPF_VENDOR_NAME_P' ) ) {
				$this->vendor_label = array( 'singular' => DPF_VENDOR_NAME_S, 'plural' => DPF_VENDOR_NAME_P );
			} else {
				$this->vendor_label = array( 'singular' => __( 'Vendor' ), 'plural' => __( 'Vendors' ) );
			}
			
			$this->orders_obj = \DPF_Orders::instance();
		}
		
		/**
		 * Returns the instance of this class.
		 *
		 * @access  public
		 * @since   2.0
		 * @return  \DPF\Dashboard
		 */
		public static function instance() {
			if ( ! isset( self::$instance ) ) {
				$className      = __CLASS__;
				self::$instance = new $className;
			}
			
			return self::$instance;
		}
		
		/**
		 * Handles all of the dashboard meta boxes to offer help to the user/admin
		 *
		 * @uses   remove_meta_box() to clean up the dashboard
		 * @uses   wp_add_dashboard_widget() to create new dashboard widgets
		 * @uses   add_meta_box() to add new dashboard meta boxes
		 *
		 * @access public
		 * @since  0.1
		 * @return void
		 */
		function manage_admin_dashboard() {
			remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );   // Right Now
			remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' ); // Recent Comments
			remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );  // Incoming Links
			remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );   // Plugins
			remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );  // Quick Press
			remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );  // Recent Drafts
			remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );   // WordPress blog
			remove_meta_box( 'dashboard_secondary', 'dashboard', 'side' );   // Other WordPress News
            remove_meta_box( 'dashboard_activity', 'dashboard', 'normal' );
            remove_meta_box( 'jetpack_summary_widget', 'dashboard', 'normal' );
            remove_meta_box( 'wpdr_dashboard', 'dashboard', 'normal' );
			
			wp_add_dashboard_widget( 'welcome-widget', sprintf( __( 'Welcome to %s' ), get_option( 'blogname' ) ), array(
				$this,
				'dashboard_welcome_widget'
			) );
			add_meta_box( 'your-info', __( 'Your Information' ), array(
				$this,
				'dashboard_info_widget'
			), 'dashboard', 'side', 'high' );
			if ( current_user_can( 'delete_users' ) ) {
				wp_add_dashboard_widget( 'adding-users', sprintf( __( 'Managing %s and %s' ), $this->reader_label['plural'], $this->vendor_label['plural'] ), array(
					$this,
					'dashboard_users_widget'
				) );
				add_meta_box( 'fair-prep', __( 'Fair Preparation' ), array(
					$this,
					'dashboard_prep_widget'
				), 'dashboard', 'side', 'high' );
			}
		}
		
		/**
		 * Output a basic widget welcoming the user to the back-end of the site
		 *
		 * @access public
		 * @since  0.1
		 * @return void
		 */
		function dashboard_welcome_widget() {
			$current_user = wp_get_current_user();
			if ( ! $current_user->exists() ) {
			    return;
            }

			$userinfo = get_userdata( $current_user->ID );
			$role     = array_shift( $userinfo->roles );
			switch ( $role ) {
				case 'author' :
					$this->dashboard_welcome_widget_author();
					break;
				case 'vendor' :
					$this->dashboard_welcome_widget_vendor();
					break;
				case 'administrator' :
					$this->dashboard_welcome_widget_author();
					$this->dashboard_welcome_widget_vendor( false );
					break;
				default :
					$this->dashboard_welcome_widget_default();
			}
		}
		
		/**
		 * Output a basic welcome widget for "readers"
		 *
		 * @param bool $show_intro whether or not to display the intro text
		 *
		 * @access public
		 * @since  0.1
		 * @return void
		 */
		function dashboard_welcome_widget_author( $show_intro = true ) {
			global $current_user;
			$bio = get_posts( array(
				'post_type'      => 'reader',
				'author'         => $current_user->ID,
				'posts_per_page' => 1,
				'numberposts'    => 1
			) );
			if ( is_array( $bio ) ) {
				$bio = array_shift( $bio );
			}
			if ( $show_intro ) {
				printf( __( '<p>Thank you for logging into the %s website.</p>' ), get_option( 'blogname' ) );
			}
			
			printf( __( '<h4>%s Information</h4>' ), $this->reader_label['singular'] );
			printf( __( '
<ol>' . "\n\t" . '<li>To manage your bio, your focus or your photograph, please <a href="%1$s">edit your %4$s bio</a>.</li>' . "\n\t" . '<li>To edit your name or any of your contact information, please <a href="%2$s">update your user profile</a>.</li>' . "\n\t" . '<li>To indicate whether or not you will be participating in the upcoming fair, please <a href="%3$s">complete the registration form</a>.</li>' . "\n" . '</ol>' ), get_edit_post_link( $bio->ID ), get_edit_user_link( $current_user->user_ID ), get_permalink( 168 ), strtolower( $this->reader_label['singular'] ) );
		}
		
		/**
		 * Output a basic welcome widget for "vendors"
		 *
		 * @param bool $show_intro whether or not to display the intro text
		 *
		 * @access public
		 * @since  0.1
		 * @return void
		 */
		function dashboard_welcome_widget_vendor( $show_intro = true ) {
			global $current_user;
			$bio = get_posts( array(
				'post_type'      => 'vendor',
				'author'         => $current_user->ID,
				'posts_per_page' => 1,
				'numberposts'    => 1
			) );
			if ( is_array( $bio ) ) {
				$bio = array_shift( $bio );
			}
			if ( $show_intro ) {
				printf( __( '<p>Thank you for logging into the %s website.</p>' ), get_option( 'blogname' ) );
			}
			
			printf( __( '<h4>%s Information</h4>' ), $this->vendor_label['singular'] );
			printf( __( '
<ol>' . "\n\t" . '<li>To manage your bio, your focus or your photograph, please <a href="%1$s">edit your %4$s bio</a>.</li>' . "\n\t" . '<li>To edit your name or any of your contact information, please <a href="%2$s">update your user profile</a>.</li>' . "\n\t" . '<li>To purchase tables for the upcoming fair, please <a href="%3$s">complete the registration form</a>.</li>' . "\n" . '</ol>' ), get_edit_post_link( $bio->ID ), get_edit_user_link( $current_user->ID ), get_permalink( 202 ), strtolower( $this->vendor_label['singular'] ) );
		}
		
		function dashboard_welcome_widget_default() {
			?>
			Thank you for visiting the <?php echo get_option( 'blogname' ) ?> website. Please <a
					href="<?php bloginfo( 'url' ) ?>">go to the home page</a> to see what information we have to offer.
			<?php
		}
		
		/**
		 * Build the main information widget for the current user
		 * @uses \DPF\Dashboard::dashboard_info_widget_reader() to build the widget for readers
		 * @uses \DPF\Dashboard::dashboard_info_widget_vendor() to build the widget for vendors
		 *
		 * @access public
		 * @since  0.1
		 * @return void
		 */
		function dashboard_info_widget() {
			$current_user = wp_get_current_user();
			$userinfo = get_userdata( $current_user->ID );
			$role     = array_shift( $userinfo->roles );
			if ( ! in_array( $role, array( 'administrator', 'author', 'vendor' ) ) ) {
				return;
			}
			
			if ( 'author' == $role || 'administrator' == $role ) {
				$this->dashboard_info_widget_reader();
				return;
			}
			if ( 'vendor' == $role/* || 'administrator' == $role*/ ) {
				$this->dashboard_info_widget_vendor();
				return;
			}
		}
		
		/**
		 * Build and output the main information widget for readers
		 *
		 * @access public
		 * @since  0.1
		 * @return void
		 */
		function dashboard_info_widget_reader() {
			$current_user = wp_get_current_user();
			$new_query = new \WP_Query( array(
				'post_type'      => 'reader',
				'author'         => $current_user->ID,
				'posts_per_page' => 1,
				'numberposts'    => 1
			) );
			if ( $new_query->have_posts() ) :
				while ( $new_query->have_posts() ) : $new_query->the_post();
					?>
					<article class="reader">
						<?php
						if ( has_post_thumbnail() ) {
							?>
							<figure class="alignright">
								<?php the_post_thumbnail( 'reader-small' ) ?>
								<figcaption><a
											href="<?php echo get_edit_post_link( get_the_ID() ) ?>#postimagediv"><?php _e( 'Edit your photo' ) ?></a>
								</figcaption>
							</figure>
							<?php
						} else {
							?>
							<figure class="alignright">
								<?php echo get_avatar( $current_user->ID, 100 ) ?>
								<figcaption><a
											href="<?php echo get_edit_post_link( get_the_ID() ) ?>#postimagediv"><?php _e( 'Edit your photo' ) ?></a>
								</figcaption>
							</figure>
							<?php
						}
						?>
						<h2><a href="<?php the_permalink() ?>"
							   title="<?php the_title_attribute() ?>"><?php echo get_the_author() ?></a></h2>
						<p>
							<a href="<?php echo get_edit_user_link( $current_user->ID ) ?>"><?php _e( 'Edit your name' ) ?></a>
						</p>
						<section class="focus">
							<h4><?php _e( 'Focus' ) ?></h4>
							<?php the_excerpt() ?>
							<p>
								<a href="<?php echo get_edit_post_link( get_the_ID() ) ?>#postexcerpt"><?php _e( 'Edit your focus' ) ?></a>
							</p>
						</section>
						<section class="bio">
							<h4><?php _e( 'Bio' ) ?></h4>
							<?php the_content() ?>
							<p>
								<a href="<?php echo get_edit_post_link( get_the_ID() ) ?>#postdivrich"><?php _e( 'Edit your bio' ) ?></a>
							</p>
						</section>
					</article>
					<?php
				endwhile;
			else :
				?>
				<p><?php _e( 'We were not able to locate a profile for you.' ) ?></p>
				<?php
			endif;
			
			wp_reset_query();
			$regform = get_post( 168 );
			if ( ! preg_match( '/id="(\d+)"/', $regform->post_content, $matches ) ) {
				return;
			}
			
			$form         = \GFFormsModel::get_form_meta( $matches[1] );
			$neededfields = array();
			$this->orders_obj->retrieve_times();
			
			foreach ( $form['fields'] as $field ) {
				
				if ( empty( $field['defaultValue'] ) && empty( $field['adminLabel'] ) ) {
					continue;
				}
				
				if ( '{user:user_login}' == $field['defaultValue'] ) {
					$neededfields['userlogin'] = (int) $field['id'];
				} elseif ( '{user:user_email}' == $field['defaultValue'] ) {
					$neededfields['useremail'] = $field['id'];
				} elseif ( 'Break Times' == $field['adminLabel'] ) {
					$neededfields['breaks'] = $field['id'];
				} elseif ( 'Participating' == $field['adminLabel'] ) {
					$neededfields['participating'] = $field['id'];
				} elseif ( 'Sold Times' == $field['adminLabel'] ) {
					$neededfields['sold'] = $field['id'];
				} elseif ( 'Reserved Times' == $field['adminLabel'] ) {
					$neededfields['reserved'] = $field['id'];
				} elseif ( 'Participating' == $field['adminLabel'] ) {
					$neededfields['participating'] = $field['id'];
				} else {
					continue;
				}
			}
			
			$entries = \GFFormsModel::get_leads( $matches[1], 1, 'DESC', '', 0, 1000 );
			
			$rt = array();
			foreach ( $entries as $entry ) {
				if ( $entry[ $neededfields['useremail'] ] != $current_user->user_email ) {
					continue;
				}
				
				if ( 'Yes, I will' != $entry[ $neededfields['participating'] ] ) {
					$msg = __( 'You have indicated that you will <strong>not</strong> be participating in the upcoming fair.' );
				} else {
					$msg      = __( 'You have indicated that you <strong>will be</strong> participating in the upcoming fair.' );
					$i        = 1;
					$sold     = array();
					$breaks   = array();
					$times    = array();
					$timekeys = array_flip( $this->orders_obj->timevalues );
					
					foreach ( $this->orders_obj->alltimes as $t => $l ) {
						$i = $timekeys[ $t ];
						if ( array_key_exists( $neededfields['sold'] . '.' . $i, $entry ) && ! empty( $entry[ $neededfields['sold'] . '.' . $i ] ) ) {
							$sold[]      = $l;
							$times[ $t ] = 1;
						} elseif ( array_key_exists( $neededfields['breaks'] . '.' . $i, $entry ) && ! empty( $entry[ $neededfields['breaks'] . '.' . $i ] ) ) {
							$breaks[]    = $l;
							$times[ $t ] = 3;
						} elseif ( array_key_exists( $neededfields['reserved'] . '.' . $i, $entry ) && ! empty( $entry[ $neededfields['reserved'] . '.' . $i ] ) ) {
							$times[ $t ] = 2;
						} elseif ( in_array( $i, $this->orders_obj->afair ) ) {
							$times[ $t ] = 4;
						} else {
							$times[ $t ] = 0;
						}
					}
				}
			}
			if ( ! isset( $msg ) ) {
				$msg = sprintf( __( 'You have not yet responded about the upcoming fair. <a href="%s">Please let us know whether or not you will be participating.</a>' ), get_permalink( 168 ) );
			}
			?>
			<article class="participating-info">
				<h2><?php _e( 'Participation Information' ) ?></h2>
				<?php echo $msg ?>
				<?php
				if ( isset( $breaks ) && ! empty( $breaks ) ) {
					?>
					<h4><?php _e( 'Requested Break Times:' ) ?></h4>
					<ul>
						<?php
						foreach ( $breaks as $b ) {
							?>
							<li><?php echo $b ?></li>
							<?php
						}
						?>
					</ul>
					<?php
				}
				if ( isset( $sold ) && ! empty( $sold ) ) {
					?>
					<h4><?php _e( 'Times That Have Been Sold:' ) ?></h4>
					<ul>
						<?php
						foreach ( $sold as $b ) {
							?>
							<li><?php echo $b ?></li>
							<?php
						}
						?>
					</ul>
					<?php
				}
				?>
			</article>
			<?php
		}
		
		/**
		 * Build and output the main information widget for "vendors"
		 *
		 * @access public
		 * @since  0.1
		 * @return void
		 */
		function dashboard_info_widget_vendor() {
			$current_user = wp_get_current_user();
			$new_query = new \WP_Query( array(
				'post_type'      => 'vendor',
				'author'         => $current_user->ID,
				'posts_per_page' => 1,
				'numberposts'    => 1
			) );
			if ( $new_query->have_posts() ) :
				while ( $new_query->have_posts() ) : $new_query->the_post();
					?>
					<article class="reader">
						<?php
						if ( has_post_thumbnail() ) {
							?>
							<figure class="alignright">
								<?php the_post_thumbnail( 'reader-small' ) ?>
								<figcaption><a
											href="<?php echo get_edit_post_link( get_the_ID() ) ?>#postimagediv"><?php _e( 'Edit your photo' ) ?></a>
								</figcaption>
							</figure>
							<?php
						} else {
							?>
							<figure class="alignright">
								<?php get_avatar( $current_user->ID, 100 ) ?>
								<figcaption><a
											href="<?php echo get_edit_post_link( get_the_ID() ) ?>#postimagediv"><?php _e( 'Edit your photo' ) ?></a>
								</figcaption>
							</figure>
							<?php
						}
						?>
						<h2><a href="<?php the_permalink() ?>"
							   title="<?php the_title_attribute() ?>"><?php echo get_the_author() ?></a></h2>
						<p>
							<a href="<?php echo get_edit_user_link( $current_user->ID ) ?>"><?php _e( 'Edit your name' ) ?></a>
						</p>
						<section class="focus">
							<h4><?php _e( 'Focus' ) ?></h4>
							<?php the_excerpt() ?>
							<p>
								<a href="<?php echo get_edit_post_link( get_the_ID() ) ?>#postexcerpt"><?php _e( 'Edit your focus' ) ?></a>
							</p>
						</section>
						<section class="bio">
							<h4><?php _e( 'Bio' ) ?></h4>
							<?php the_content() ?>
							<p>
								<a href="<?php echo get_edit_post_link( get_the_ID() ) ?>#postdivrich"><?php _e( 'Edit your bio' ) ?></a>
							</p>
						</section>
					</article>
					<?php
				endwhile;
			else :
				?>
				<p><?php _e( 'We were not able to locate a profile for you.' ) ?></p>
				<?php
			endif;
			
			wp_reset_query();
			
			$regform = get_post( 202 );
			if ( ! preg_match( '/id="(\d+)"/', $regform->post_content, $matches ) ) {
				return;
			}
			
			$form         = \GFFormsModel::get_form_meta( $matches[1] );
			$neededfields = array();
			
			$neededfields['email']   = 8;
			$neededfields['tables1'] = 4;
			$neededfields['tables2'] = 6;
			$neededfields['tables3'] = 7;
			$neededfields['tables4'] = 17;
			
			$entries = \GFFormsModel::get_leads( $matches[1], 1, 'DESC', '', 0, 1000 );
			
			foreach ( $entries as $entry ) {
				if ( $entry[ $neededfields['email'] ] != $current_user->user_email ) {
					continue;
				}
				
				$tables = null;
				if ( array_key_exists( $neededfields['tables4'], $entry ) && ! empty( $entry[ $neededfields['tables4'] ] ) ) {
				    $tmp = $entry[ $neededfields['tables4'] ];
				    if ( stristr( $tmp, 'One Table' ) ) {
				        $tables = 1;
                    } else if ( stristr( $tmp, 'Two Tables' ) ) {
				        $tables = 2;
                    } else if ( stristr( $tmp, 'Three Tables' ) ) {
				        $tables = 3;
                    }
				} elseif ( array_key_exists( $neededfields['tables1'] . '.0', $entry ) && ! empty( $entry[ $neededfields['tables1'] . '.0' ] ) ) {
					$tables = 1;
				} elseif ( array_key_exists( $neededfields['tables1'] . '.1', $entry ) && ! empty( $entry[ $neededfields['tables1'] . '.1' ] ) ) {
					$tables = 2;
				} elseif ( array_key_exists( $neededfields['tables1'] . '.1', $entry ) && ! empty( $entry[ $neededfields['tables1'] . '.1' ] ) ) {
					$tables = 3;
				} elseif ( array_key_exists( $neededfields['tables2'] . '.0', $entry ) && ! empty( $entry[ $neededfields['tables1'] . '.0' ] ) ) {
					$tables = 1;
				} elseif ( array_key_exists( $neededfields['tables2'] . '.1', $entry ) && ! empty( $entry[ $neededfields['tables1'] . '.1' ] ) ) {
					$tables = 2;
				} elseif ( array_key_exists( $neededfields['tables2'] . '.1', $entry ) && ! empty( $entry[ $neededfields['tables1'] . '.1' ] ) ) {
					$tables = 3;
				} elseif ( array_key_exists( $neededfields['tables3'] . '.0', $entry ) && ! empty( $entry[ $neededfields['tables1'] . '.0' ] ) ) {
					$tables = 1;
				} elseif ( array_key_exists( $neededfields['tables3'] . '.1', $entry ) && ! empty( $entry[ $neededfields['tables1'] . '.1' ] ) ) {
					$tables = 2;
				} elseif ( array_key_exists( $neededfields['tables3'] . '.1', $entry ) && ! empty( $entry[ $neededfields['tables1'] . '.1' ] ) ) {
					$tables = 3;
				}
			}
			
			if ( empty( $tables ) ) {
				$msg = sprintf( __( 'It does not appear that you have purchased any tables for the upcoming fair. If you would like to, <a href="%s">please submit your order form.</a>' ), get_permalink( 202 ) );
			} else {
				$msg = sprintf( __( 'You have purchased %d tables for the upcoming fair. Thank you.' ), $tables );
			}
			?>
			<article class="participating-info">
				<h2><?php _e( 'Participation Information' ) ?></h2>
				<?php echo $msg ?>
			</article>
			<?php
		}
		
		/**
		 * Output the dashboard widget with fair preparation instructions
		 *
		 * @access public
		 * @since  0.1
		 * @return void
		 */
		function dashboard_prep_widget() {
			$current_user = wp_get_current_user();
			?>
			<h2><?php printf( __( 'Greetings, %s!' ), $current_user->display_name ) ?></h2>
			<p><?php _e( 'Following are some quick instructions to set up for the next fair.' ) ?></p>
            <?php
            $this->reader_registration_widget();
            $this->vendor_registration_widget();
            $this->workshop_registration_widget();
            $this->front_page_setup_widget();
            ?>
			<h4><?php _e( 'General Cleanup' ) ?></h4>
			<ol>
				<li><?php printf( __( '<a href="%s">Visit the list of "active" forms.</a>' ), admin_url( 'admin.php?page=gf_edit_forms&active=1' ) ) ?></li>
				<li><?php _e( 'Select all of the previous fair\'s forms.' ) ?></li>
				<li><?php _e( 'Choose "Mark as Inactive" from the "Bulk Actions" menu and click the "Apply" button.' ) ?></li>
			</ol>
			<?php
		}
		
		/**
		 * Output the instructions for updating the Reader registration form
         *
         * @access private
         * @since  2.0.6
         * @return void
		 */
		private function reader_registration_widget() {
?>
            <h4><?php printf( __( '%s Registration' ), $this->reader_label['singular'] ) ?></h4>
            <ol>
                <li><?php printf( __( 'Please <a href="%1$s">visit the Forms page</a> and duplicate the most recent "%2$s Participation" form.' ), admin_url( 'admin.php?page=gf_edit_forms&active=1' ), $this->reader_label['singular'] ) ?></li>
                <li><?php _e( 'Edit the new form:' ) ?>
                    <ol>
                        <li><?php printf( __( 'Edit the "Fair Date" field by clicking on it, then visiting the "Advanced" tab for that field; change the default value to the next fair\'s date (%s). Click the "Update" button to save the changes you made to the form.' ), date( 'M d, Y', $this->orders_obj->fairdate ) ) ?></li>
                        <li><?php _e( 'Click "Form Settings" and change the "Form title" to reflect the new fair date.' ) ?></li>
                    </ol>
                </li>
                <li><?php printf( __( '<a href="%s">Edit the page that contains the form</a>' ), get_edit_post_link( 168 ) ) ?>
                    <ol>
                        <li><?php _e( 'Highlight all of the content in the editor and delete it.' ) ?></li>
                        <li><?php _e( 'Click on the "Add Form" button above the editor.' ) ?></li>
                        <li><?php _e( 'Select the new form you created from the dropdown menu, uncheck the "Display form title" and "Display form description" boxes, and press the "Insert Form" button.' ) ?></li>
                        <li><?php _e( 'Click the "Update" button to save the page.' ) ?></li>
                    </ol>
                </li>
            </ol>

<?php
        }
		
		/**
		 * Output the instructions for updating the Vendor registration form
         *
         * @access private
         * @since  2.0.6
         * @return void
		 */
		private function vendor_registration_widget() {
		    if ( ! defined( 'FAIR_VENDOR_DISCOUNT_STRUCTURE' ) ) {
		        define( 'FAIR_VENDOR_DISCOUNT_STRUCTURE', 3 );
            }
?>
            <h4><?php printf( __( '%s Registration' ), $this->vendor_label['singular'] ) ?></h4>
            <ol>
                <li><?php printf( __( 'Please <a href="%1$s">visit the Forms page</a> and duplicate the most recent "%2$s Registration" form.' ), admin_url( 'admin.php?page=gf_edit_forms&active=1' ), $this->vendor_label['singular'] ) ?></li>
                <li><?php _e( 'Edit the new form:' ) ?>
                    <ol>
                        <li><?php printf( __( 'Edit the "Fair Date" field by clicking on it, then updating the date under "Options" to read %s. Click the "Save" button to save the changes.' ), date( 'M d, Y', $this->orders_obj->fairdate ) ) ?></li>
                        <li><?php _e( 'Click "Form Settings" and change the "Form title" to reflect the new fair date.' ) ?></li>
                        <li><?php _e( 'Hover over "Form Settings" and click "Conditional Pricing"' ); ?></li>
                        <li><?php _e( 'Expand the "Early Registration Discount"' ); ?></li>
                        <?php
                        switch( FAIR_VENDOR_DISCOUNT_STRUCTURE ) {
                            case 1 :
                                ?>
                        <li><?php _e( 'If you click the down arrow (<span class="dashicons dashicons-arrow-down"></span>) next to "Early Registration Discount", you will see two different pricing levels:' ) ?>
                            <ol>
                                <li><?php printf( __( 'The first is the late registration, which means there is <em>no discount</em>/full price (the item will indicate that the early registration discount "costs $0"). Edit that level to take effect if the date is <em>greater than</em> one week prior to the fair (%s). Make sure that the date is in mm/dd/yyyy format; e.g. %s' ), date( 'm/d/Y', strtotime( $this->orders_obj->fairdate . " -1 week" ) ), date( "m/d/Y" ) ); ?></li>
                                <li><?php printf( __( 'The second level is the early registration level. Currently, that discount is set to <em>subtract $10</em> from the cost of the tables. Ensure that the "This product costs" field is set to a negative number (e.g. "-10") to properly apply the discount. Edit that level so that it takes effect if the date is <em>less than</em> 6 days prior to the fair (%s). Make sure that the date is in mm/dd/yyyy format; e.g. %s' ), date( 'm/d/Y', strtotime( $this->orders_obj->fairdate . " -6 days" ) ), date( "m/d/Y" ) ) ?></li>
                            </ol>
                        </li>
                                <?php
                                break;
                            default :
                        ?>
                        <li><?php _e( 'If you click the down arrow (<span class="dashicons dashicons-arrow-down"></span>) next to "Early Registration Discount", you will see three different pricing levels:' ) ?>
                            <ol>
                                <li><?php printf( __( 'The first is the late registration, which means there is <em>no discount</em> (the item will indicate that the early registration discount "costs $0"). Edit that level to take effect if the date is <em>greater than</em> the 20th of the month (%s). Make sure that the date is in mm/dd/yyyy format; e.g. %s' ), date( "m/20/Y" ), date( "m/d/Y" ) ); ?></li>
                                <li><?php printf( __( 'The second level is the second tier discount. Currently, that discount is set to <em>subtract $25</em> from the cost of the tables. Ensure that the "This product costs" field is set to a negative number (e.g. "-25") to properly apply the discount. Edit that level so that it takes effect if the date is <em>less than</em> the 21st of the month (%s) <strong>and</strong> <em>greater than</em> the 10th of the month (%s). Make sure that the date is in mm/dd/yyyy format; e.g. %s' ), date( 'm/21/Y' ), date( 'm/10/Y' ), date( "m/d/Y" ) ) ?></li>
                                <li><?php printf( __( 'The third level is the first tier discount; <em>subtracting $50</em> from the cost of the tables. Ensure that the "This product costs" field is set to a negative number (e.g. "-50") to properly apply the discount. Edit that level so that it takes effect if the date is <em>less than</em> the 11th of the month (%s). Make sure that the date is in mm/dd/yyyy format; e.g. %s' ), date( 'm/11/Y' ), date( "m/d/Y" ) ) ?></li>
                            </ol>
                        </li>
                                <?php
	                            break;
                        }
                        ?>
                        <li><?php _e( 'Click the "Save Conditional Pricing" button to save these changes.' ) ?></li>
                    </ol>
                </li>
                <li><?php printf( __( 'Set up a new Authorize.net "feed" to collect payments for %s registrations.' ), $this->vendor_label['singular'] ) ?>
                    <ol>
                        <li><?php _e( 'While in the "Settings" area for the form you created, click "Authorize.net" on the left side.' ) ?></li>
                        <li><?php _e( 'Click the "Add New" button.' ) ?></li>
                        <li><?php _e( 'Choose "Products and Services" as the Transaction Type' ) ?></li>
                        <li><?php _e( 'For the "Payment Amount" option, choose "Form Total" ' ) ?></li>
                        <li><?php _e( 'Match up the appropriate form fields with the Authorize.net fields. (Most of these will already be matched appropriately)' ) ?></li>
                        <li><?php _e( 'Check the "Send Authorize.net email receipt" button.' ) ?></li>
                        <li><?php _e( 'Click the "Update Settings" button.' ) ?></li>
                    </ol>
                </li>
                <li><?php printf( __( '<a href="%s">Edit the page that contains the form</a>' ), get_edit_post_link( 202 ) ) ?>
                    <ol>
                        <li><?php _e( 'Highlight all of the content in the editor and delete it.' ) ?></li>
                        <li><?php _e( 'Click on the "Add Form" button above the editor.' ) ?></li>
                        <li><?php _e( 'Select the new form you created from the dropdown menu, uncheck the "Display form title" and "Display form description" boxes, and press the "Insert Form" button.' ) ?></li>
                        <li><?php _e( 'Click the "Update" button to save the page.' ) ?></li>
                    </ol>
                </li>
            </ol>
<?php
        }
		
		/**
		 * Output the instructions for setting up the workshop registration form
         *
         * @access private
         * @since  2.0.6
         * @return void
		 */
		private function workshop_registration_widget() {
		    if ( defined( 'FAIR_NO_WORKSHOPS' ) && true === FAIR_NO_WORKSHOPS ) {
		        return;
            }
?>
            <h4><?php _e( 'Classes &amp; Workshops' ) ?></h4>
            <ol>
                <li><?php printf( __( 'Please <a href="%s">visit the Forms page</a> and duplicate the most recent "Classes &amp; Workshops" form.' ), admin_url( 'admin.php?page=gf_edit_forms&active=1' ) ) ?></li>
                <li><?php _e( 'Edit the new form:' ) ?>
                    <ol>
                        <li><?php printf( __( 'Edit each of the class/workshop descriptions. <p><strong>Tip:</strong> If you would like to format the content of each description:</p><ol><li>Open a new tab or window in your browser</li><li><a href="%s" target="_blank">create a new post</a></li><li>Edit your content within the blank editor</li><li>Click the "HTML" tab</li><li>Copy all of the content of the HTML editor</li><li>Return to the form editor</li><li>Paste the copied content into the description field</li></ol>' ), admin_url( 'post-new.php' ) ) ?></li>
                        <li><?php _e( 'Click "Form Settings" and change the "Form title" to reflect the new fair date.' ) ?></li>
                    </ol>
                </li>
                <li><?php _e( 'Set up a new Authorize.net "feed" to collect payments for workshop registrations.' ) ?>
                    <ol>
                        <li><?php _e( 'While in the "Settings" area for the form you created, click "Authorize.net" on the left side.' ) ?></li>
                        <li><?php _e( 'Click the "Add New" button.' ) ?></li>
                        <li><?php _e( 'Choose "Products and Services" as the Transaction Type' ) ?></li>
                        <li><?php _e( 'For the "Payment Amount" option, choose "Form Total" ' ) ?></li>
                        <li><?php _e( 'Match up the appropriate form fields with the Authorize.net fields. (Most of these will already be matched appropriately)' ) ?></li>
                        <li><?php _e( 'Check the "Send Authorize.net email receipt" button.' ) ?></li>
                        <li><?php _e( 'Click the "Update Settings" button.' ) ?></li>
                    </ol>
                </li>
                <li><?php printf( __( '<a href="%s">Edit the page that contains the form</a>' ), get_edit_post_link( 13 ) ) ?>
                    <ol>
                        <li><?php _e( 'Highlight all of the content in the editor and delete it.' ) ?></li>
                        <li><?php _e( 'Click on the "Add Form" button above the editor.' ) ?></li>
                        <li><?php _e( 'Select the new form you created from the dropdown menu, uncheck the "Display form title" and "Display form description" boxes, and press the "Insert Form" button.' ) ?></li>
                        <li><?php _e( 'Click the "Update" button to save the page.' ) ?></li>
                    </ol>
                </li>
            </ol>
<?php
        }
		
		/**
		 * Output the instructions for updating the front page of the website
         *
         * @access private
         * @since  2.0.6
         * @return void
		 */
		private function front_page_setup_widget() {
?>
            <h4><?php _e( 'Front Page' ) ?></h4>
            <ol>
                <li><?php printf( __( '<a href="%s">Visit the front page.</a>' ), get_home_url() ) ?></li>
                <li><?php printf( __( 'Click the <a href="%s">Customize link</a> in the Admin Bar at the top of the screen.' ), add_query_arg( 'url', urlencode( get_home_url() ), wp_customize_url() ) ) ?></li>
                <li><?php _e( 'Hold down the Shift button and click on the list of dates under the "Upcoming Fairs" heading near the bottom of the page.' ) ?></li>
                <li><?php _e( 'Edit the list of dates in the customizer window and click Save.' ) ?></li>
            </ol>
<?php
        }
		
		/**
		 * Output a widget with instructions explaining how to create new users
		 *
		 * @access public
		 * @since  0.1
		 * @return void
		 */
		function dashboard_users_widget() {
			?>
			<h2><?php printf( __( 'Adding New %s' ), $this->vendor_label['plural'] ) ?></h2>
			<ul>
				<li><?php printf( __( 'To add a new %2$s to the site, begin by going to <a href="%1$s">Users -> Add New.</a>' ), admin_url( 'user-new.php' ), strtolower( $this->vendor_label['singular'] ) ) ?></li>
				<li><?php printf( __( 'Fill in the Username, Email Address and Password/Repeat Password for the new user. You can <a href="%s" target="_blank">generate a random password for the user</a>, copy the new password, and paste it into the two password fields.' ), 'http://www.pctools.com/guides/password/?length=13&phonetic=on&alpha=on&mixedcase=on&numeric=on&nosimilar=on&quantity=1&generate=true#password_generator' ); ?></li>
				<li><?php printf( __( 'Check the "Send this password to the new user by email." box and choose "%1$s" from the "Role" box.' ), $this->vendor_label['singular'] ) ?></li>
				<li><?php _e( 'Click the "Add New User" button' ) ?></li>
				<li>
					<strong><?php printf( __( 'Adding %1$s Profile Information' ), $this->vendor_label['singular'] ) ?></strong>
				</li>
				<li><?php printf( __( '<a href="%1$s">Create a new "%2$s" post.</a>' ), admin_url( 'post-new.php?post_type=vendor' ), $this->vendor_label['singular'] ) ?></li>
				<li><?php printf( __( 'Enter the %1$s name as the title of the post.' ), strtolower( $this->vendor_label['singular'] ) ) ?></li>
				<li><?php printf( __( 'Enter the %1$s\'s focus in the "Excerpt" box.' ), strtolower( $this->vendor_label['singular'] ) ) ?></li>
				<li><?php _e( 'Locate the new vendor user account within the "Author" selector and select it.' ) ?></li>
				<li><?php printf( __( 'Click the "Set Featured Image" link on the right side of the page; locate the %1$s\'s photo on your computer and drag it into the page. Click the "Set Featured Image" button in the bottom right.' ), strtolower( $this->vendor_label['singular'] ) ) ?></li>
				<li><?php _e( 'Click the "Publish" button.' ) ?></li>
				<li>
					<strong><?php printf( __( 'Adding %1$s Contact Information' ), $this->vendor_label['singular'] ) ?></strong>
				</li>
				<li><?php printf( __( 'Locate the new vendor in <a href="%1$s">the list of all %2$s users.</a>' ), admin_url( 'users.php?role=vendor' ), strtolower( $this->vendor_label['singular'] ) ) ?></li>
				<li><?php printf( __( 'Click on the %1$s\'s username.' ), strtolower( $this->vendor_label['singular'] ) ) ?></li>
				<li><?php printf( __( 'Fill in the %1$s\'s First Name and Last Name, then add the company name (if applicable) as the Nickname.' ), strtolower( $this->vendor_label['singular'] ) ) ?></li>
				<li><?php _e( 'Choose the format you prefer (most likely either the Nickname/company name or First Name then Last Name) in the "Display name publicly as" box.' ) ?></li>
				<li><?php printf( __( 'Enter the %1$s\'s website address and telephone number, if applicable under the "Contact Info" heading.' ), strtolower( $this->vendor_label['singular'] ) ) ?></li>
				<li><?php _e( 'Click the "Update User" button.' ) ?></li>
			</ul>

			<h2><?php printf( __( 'Adding New %1$s' ), $this->reader_label['plural'] ) ?></h2>
			<ul>
				<li><?php printf( __( 'To add a new %2$s to the site, begin by going to <a href="%1$s">Users -> Add New.</a>' ), admin_url( 'user-new.php' ), strtolower( $this->reader_label['singular'] ) ) ?></li>
				<li><?php printf( __( 'Fill in the Username, Email Address and Password/Repeat Password for the new user. You can <a href="%1$s" target="_blank">generate a random password for the user</a>, copy the new password, and paste it into the two password fields.' ), 'http://www.pctools.com/guides/password/?length=13&phonetic=on&alpha=on&mixedcase=on&numeric=on&nosimilar=on&quantity=1&generate=true#password_generator' ); ?></li>
				<li><?php printf( __( 'Check the "Send this password to the new user by email." box and choose "%1$s" from the "Role" box.' ), $this->reader_label['singular'] ) ?></li>
				<li><?php _e( 'Click the "Add New User" button' ) ?></li>
				<li>
					<strong><?php printf( __( 'Adding %1$s Profile Information' ), $this->reader_label['singular'] ) ?></strong>
				</li>
				<li><?php printf( __( '<a href="%1$s">Create a new "%2$s" post.</a>' ), admin_url( 'post-new.php?post_type=reader' ), $this->reader_label['singular'] ) ?></li>
				<li><?php printf( __( 'Enter the %1$s\'s last name, a comma, then the %1$s\'s first name as the title of the post (this is critical in order to ensure that the readers appear in alphabetical order according to last name).' ), strtolower( $this->reader_label['singular'] ) ) ?></li>
				<li><?php printf( __( 'Enter the %1$s\'s focus in the "Excerpt" box.' ), strtolower( $this->reader_label['singular'] ) ) ?></li>
				<li><?php _e( 'Locate the new reader user account within the "Author" selector and select it.' ) ?></li>
				<li><?php printf( __( 'Click the "Set Featured Image" link on the right side of the page; locate the %1$s\'s photo on your computer and drag it into the page. Click the "Set Featured Image" button in the bottom right.' ), strtolower( $this->reader_label['singular'] ) ) ?></li>
				<li><?php _e( 'Click the "Publish" button.' ) ?></li>
				<li>
					<strong><?php printf( __( 'Adding %1$s Contact Information' ), $this->reader_label['singular'] ) ?></strong>
				</li>
				<li><?php printf( __( 'Locate the new %2$s in <a href="%1$s">the list of all author users.</a>' ), admin_url( 'users.php?role=author' ), strtolower( $this->reader_label['singular'] ) ) ?></li>
				<li><?php printf( __( 'Click on the %1$s\'s username.' ), strtolower( $this->reader_label['singular'] ) ) ?></li>
				<li><?php printf( __( 'Fill in the %1$s\'s First Name and Last Name. You can leave the Nickname as it is.' ), strtolower( $this->reader_label['singular'] ) ) ?></li>
				<li><?php _e( 'Choose the format you prefer (most likely First Name then Last Name) in the "Display name publicly as" box.' ) ?></li>
				<li><?php printf( __( 'Enter the %1$s\'s website address and telephone number, if applicable under the "Contact Info" heading.' ), strtolower( $this->reader_label['singular'] ) ) ?></li>
				<li><?php _e( 'Click the "Update User" button.' ) ?></li>
			</ul>
			<?php
		}
	}
	
}