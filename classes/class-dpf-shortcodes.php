<?php
/**
 * Implements the various shortcodes used throughout the Dallas Psychic Fair
 *      model sites
 *
 * @package    WordPress
 * @subpackage DPF Reservations
 * @version    2.0
 */

namespace {
	if ( ! defined( 'ABSPATH' ) ) {
		die( 'You do not have permission to access this file directly.' );
	}
}

namespace DPF {
	class Shortcodes {
		/**
		 * @var \DPF\Shortcodes $instance holds the single instance of this class
		 * @access private
		 */
		private static $instance;

		/**
		 * @var string $version holds the version number for the plugin
		 * @access public
		 */
		public $version = '2019.08';

		/**
		 * @var \DPF_Orders|null holds the instance of the DPF_Orders object that
		 *                       contains some methods/properties we need to use
		 */
		private $orders_obj = null;


		/**
		 * Creates the \DPF\Shortcodes object
		 *
		 * @access private
		 * @since  2.0
		 */
		private function __construct() {
			add_shortcode( 'dpf-accordion', array( $this, 'accordion' ) );
			add_shortcode( 'reservation-form', array( $this, 'reader_reservation_form' ) );
			add_shortcode( 'active-reader-list', array( $this, 'get_registered_readers' ) );
			add_shortcode( 'active-vendor-list', array( $this, 'get_registered_vendors' ) );
			add_shortcode( 'fair-date', array( $this, 'do_fair_date_shortcode' ) );

			$this->orders_obj = \DPF_Orders::instance();
		}

		/**
		 * Returns the instance of this class.
		 *
		 * @access  public
		 * @return  \DPF\Shortcodes
		 * @since   2.0
		 */
		public static function instance() {
			if ( ! isset( self::$instance ) ) {
				$className      = __CLASS__;
				self::$instance = new $className;
			}

			return self::$instance;
		}

		/**
		 * Render the accordion shortcode
		 *
		 * @param array $atts the array of shortcode attributes
		 *
		 * @access public
		 * @return string the rendered HTML content
		 * @since  0.1
		 */
		function accordion( $atts = array() ) {
			global $post;
			if ( ! isset( $post ) || ! is_object( $post ) ) {
				return '';
			}

			$defaults = array(
				'post_type'      => 'page',
				'orderby'        => 'menu_order title',
				'order'          => 'ASC',
				'posts_per_page' => - 1,
				'numberposts'    => - 1,
			);

			if ( empty( $atts ) ) {
				$defaults['post_parent'] = $post->ID;
			}

			$atts = shortcode_atts( $defaults, $atts );

			$children = new \WP_Query( $atts );
			if ( ! $children->have_posts() ) {
				return '';
			}

			$rt = '
	<section class="madc-accordion">';
			while ( $children->have_posts() ) : $children->the_post();
				$rt .= '
		<h3 class="accordion-title"><a href="#post-' . get_the_ID() . '">' . apply_filters( 'the_title', get_the_title(), get_the_ID() ) . '</a></h3>
		<div class="accordion-content" id="post-' . get_the_ID() . '">' . apply_filters( 'the_content', get_the_content() ) . '</div>';
			endwhile;
			$rt .= '
	</section>';

			wp_reset_postdata();

			return $rt;
		}

		/**
		 * Generate the form that's used for reserving readings
		 *
		 * @param array $atts the array of shortcode attributes
		 *
		 * @access public
		 * @return string the rendered HTML content
		 * @since  0.1
		 */
		function reader_reservation_form( $atts = array() ) {
			$is_open = $this->is_order_form_active();

			if ( 'notopen' == $is_open ) {
				return apply_filters( 'dpf-not-open-yet', sprintf( __( '<p><em>This order form is currently closed. It will open at <strong>%s on %s</strong>. Thank you.</em></p>' ), date( $this->orders_obj->timeformat, $this->orders_obj->livedate ), date( $this->orders_obj->dateformat, $this->orders_obj->livedate ) ) );
			}
			if ( 'closed' == $is_open ) {
				return apply_filters( 'dpf-not-open-yet', sprintf( __( '<p><em>This order form has closed until the next fair. You will be able to reserve spots for our next fair at <strong>%s on %s</strong>. Thank you.</em></p>' ), date( $this->orders_obj->timeformat, $this->orders_obj->nextopen ), date( $this->orders_obj->dateformat, $this->orders_obj->nextopen ) ) );
			}

			wp_register_script( 'nyroModal', plugins_url( '/scripts/jquery.nyroModal/js/jquery.nyroModal.custom.min.js', dirname( __FILE__ ) ), array( 'jquery' ), '0.1', true );
			wp_enqueue_script( 'dpf-reservations', plugins_url( '/scripts/dpf-reservations.js', dirname( __FILE__ ) ), array(
				'jquery',
				'nyroModal'
			), $this->version, true );
			wp_localize_script( 'dpf-reservations', 'dpf_ajax_object', array(
				'ajax_url' => admin_url( 'admin-ajax.php', 'https' ),
				'action'   => 'dpf_reserve_spots'
			) );

			return $this->get_form();
		}

		/**
		 * Determine whether the order process should be active/open right now
		 *
		 * @access private
		 * @return string live|closed|notopen
		 * @since  2019.08
		 */
		private function is_order_form_active() {
			if ( is_user_logged_in() && current_user_can( 'delete_users' ) ) {
				return 'live';
			}
			if ( current_time( 'timestamp' ) < $this->orders_obj->livedate ) {
				return 'notopen';
			}
			if ( current_time( 'timestamp' ) > $this->orders_obj->closedate ) {
				return 'closed';
			}

			return 'live';
		}

		/**
		 * Build the reservation form
		 *
		 * @access public
		 * @return string the rendered form HTML
		 * @since  0.1
		 */
		function get_form() {
			$rt = '
<form id="reading-selection">
	<ul class="readers">';

			$times        = $this->orders_obj->reserve_times();
			$readerblocks = array();
			foreach ( $this->orders_obj->readers as $readerid => $reader ) {
				$readername = array(
					'first' => get_user_meta( $readerid, 'first_name', true ),
					'last'  => get_user_meta( $readerid, 'last_name', true )
				);

				$rkey = $readername['last'] . $readername['first'];
				if ( $reader->is_vendor ) {
					$rkey = 'zzzz_' . $rkey;
				}

				$readerblocks[ $rkey ] = '
		<li>';
				$readerblocks[ $rkey ] .= $reader->summary;
				$readerblocks[ $rkey ] .= '
			<ul class="times">';

				foreach ( $reader->times as $serial => $booked ) {
					$status   = $booked;
					$reserved = get_transient( 'openings_' . $readerid . '_' . $serial );
					switch ( $booked ) {
						case 1 :
							$message = __( 'Sold' );
							break;
						case 2 :
							$message = __( 'Reserved' );
							break;
						case 3 :
							$message = __( 'On Break' );
							break;
						case 4 :
							$message = __( 'Available at Fair' );
							break;
						default :
							if ( false !== $reserved ) {
								$booked  = 2;
								$message = __( 'Reserved' );
							} else {
								$message = __( 'Available' );
							}
							break;
					}
					$message = '<span rel="timename">' . $this->orders_obj->alltimes[ $serial ] . '</span> - ' . $message;

					$readerblocks[ $rkey ] .= '
				<li class="time-slot ' . ( 3 == $status || 1 == $status || 4 == $status ? 'unavailable' : ( 2 == $status ? 'reserved' : 'available' ) ) . '" rel="reader-' . $readerid . '-' . $serial . '">';
					switch ( $status ) {
						case 1 :
						case 2 :
						case 3 :
						case 4 :
							$readerblocks[ $rkey ] .= $message;
							break;
						default :
							$readerblocks[ $rkey ] .= '<input type="checkbox" class="readings-checkbox" name="readings[' . $readerid . '][' . $serial . ']" value="' . $serial . '" id="readings_' . esc_attr( $readerid ) . '_' . $serial . '"' . checked( 5 === $status, true, false ) . '/> <label for="readings_' . esc_attr( $readerid ) . '_' . $serial . '">' . $message . '</label>';
							break;
					}
					$readerblocks[ $rkey ] .= '
				</li>';
				}

				$readerblocks[ $rkey ] .= '
			</ul>';
				$readerblocks[ $rkey ] .= '
		</li>';
			}

			ksort( $readerblocks );
			$vendorblocks = array();
			foreach ( $readerblocks as $k => $v ) {
				if ( stristr( $k, 'zzzz_' ) ) {
					$vendorblocks[ $k ] = $v;
					unset( $readerblocks[ $k ] );
				}
			}
			$rt .= implode( '', $readerblocks );

			if ( ! empty( $vendorblocks ) ) {
				$rt .= '
		<li style="clear: both; height: 0; width: 0; line-height: 0; overflow: hidden; float: none; padding: 0; margin: 0;">&nbsp;</li>
	</ul>';

				$rt .= '<h2 style="float: none; clear: both;">Other Practitioners</h2>';
				$rt .= '<ul class="readers">';

				$rt .= implode( '', $vendorblocks );
			}

			$rt .= '
		<li style="clear: both; height: 0; width: 0; line-height: 0; overflow: hidden; float: none; padding: 0; margin: 0;">&nbsp;</li>
	</ul>
</form>';

			$rt .= '<form method="get" action="' . get_permalink( 6 ) . '" id="order-form"><input type="hidden" name="sessionid" class="sessionid" value="' . ( isset( $_GET['sessionid'] ) ? esc_attr( $_GET['sessionid'] ) : uniqid() ) . '"/><input type="hidden" name="readings-text" value=""/><input type="hidden" name="readings" value=""/><input type="hidden" name="total" value=""/><input type="hidden" name="qty" value=""/><input class="reservation-checkout" type="submit" value="Continue to Checkout"/></form>';

			return $rt;
		}

		/**
		 * Output the reservation form
		 *
		 * @access public
		 * @return void
		 * @since  0.1
		 */
		function form() {
			echo $this->get_form();
		}

		/**
		 * Get Registered Readers
		 *
		 * @access public
		 * @return string the rendered HTML for the reader list
		 * @since  0.1
		 */
		function get_registered_readers() {
			$rt      = '';
			$regform = get_post( 168 );
			if ( ! preg_match( '/id="(\d+)"/', $regform->post_content, $matches ) ) {
				return $rt;
			}

			$form         = \GFFormsModel::get_form_meta( $matches[1] );
			$neededfields = array();

			/*print( '<pre><code>' );
			var_dump( $form );
			print( '</code></pre>' );*/

			foreach ( $form['fields'] as $field ) {

				/*print( '<pre><code>' );
				var_dump( $field );
				print( '</code></pre>' );*/

				if ( empty( $field['defaultValue'] ) && empty( $field['adminLabel'] ) ) {
					continue;
				}

				if ( '{user:user_login}' == $field['defaultValue'] ) {
					$neededfields['userlogin'] = (int) $field['id'];
					/*print( '<pre><code>We set the userlogin value to ' . (int) $field['id'] . '</code></pre>' );*/
				} elseif ( '{user:user_email}' == $field['defaultValue'] ) {
					$neededfields['useremail'] = $field['id'];
					/*print( '<pre><code>We set the useremail value to ' . (int) $field['id'] . '</code></pre>' );*/
				} elseif ( 'Break Times' == $field['adminLabel'] ) {
					$neededfields['breaks'] = $field['id'];
					/*print( '<pre><code>We set the breaks value to ' . (int) $field['id'] . '</code></pre>' );*/
				} elseif ( 'Participating' == $field['adminLabel'] ) {
					$neededfields['participating'] = $field['id'];
				} else {
					/*print( '<pre><code>The field that looks like the following did not match the fields we were searching for' . "\n" );
					var_dump( $entry );
					print( '</code></pre>' );*/
					continue;
				}
			}

			/*$entries = \GFFormsModel::get_leads( $matches[1], 1, 'DESC', '', 0, 1000 );*/
			$entries = \GFAPI::get_entries( $matches[1], array( 'status' => 'active' ), array( 'key'        => 1,
			                                                             'direction'  => 'DESC',
			                                                             'is_numeric' => false
			), array( 'page_size' => 1000 ) );

			$rt = array();

			foreach ( $entries as $entry ) {
				if ( 'Yes, I will' != $entry[ $neededfields['participating'] ] ) {
					continue;
				}

				/*print( '<pre><code>' );
				var_dump( $entry );
				print( '</code></pre>' );*/

				if ( ! array_key_exists( 'userlogin', $neededfields ) || empty( $neededfields['userlogin'] ) ) {
					/*print( '<pre><code>The userlogin field seems empty</code></pre>' );*/
					continue;
				}

				$user   = get_user_by( 'email', $entry[ $neededfields['useremail'] ] );
				$reader = get_posts( array(
					'author'         => $user->ID,
					'post_type'      => 'reader',
					'post_status'    => 'publish',
					'posts_per_page' => 1,
				) );
				if ( ! is_array( $reader ) || is_wp_error( $reader ) || empty( $reader ) ) {
					/*print( '<pre><code>' );
					var_dump( $user );
					print( '</code></pre>' );*/
					continue;
				}

				/*print( '<pre><code>' );
				var_dump( $user );
				print( '</code></pre>' );
				continue;*/

				$reader     = array_shift( $reader );
				$readername = array(
					'first' => get_user_meta( $user->ID, 'first_name', true ),
					'last'  => get_user_meta( $user->ID, 'last_name', true )
				);

				if ( empty( $readername['first'] ) && empty( $readername['last'] ) ) {
					/*print( '<pre><code>' );
					var_dump( $user );
					print( '</code></pre>' );*/
					continue;
				}

				$rt[ $readername['last'] . '-' . $readername['first'] ] = '
<article class="active-reader">';

				if ( has_post_thumbnail( $reader->ID ) ) {
					$avatar_id = get_post_thumbnail_id( $reader->ID );
					list( $avatar ) = wp_get_attachment_image_src( $avatar_id, 'medium', false );
				} else {
					$avatar = get_avatar_url( $user->ID, array( 'size' => 300, 'default' => 'mystery' ) );
				}
				$rt[ $readername['last'] . '-' . $readername['first'] ] .= sprintf( '<div class="reader-avatar" style="background-image:url(%s)"><div class="reader-info">', $avatar );

				$rt[ $readername['last'] . '-' . $readername['first'] ] .= '
	<h2 class="reader-name">
		<a href="' . get_permalink( $reader->ID ) . '" title="View the bio of ' . $readername['first'] . ' ' . $readername['last'] . '">
			' . $readername['first'] . ' ' . $readername['last'] . '
		</a>
	</h2>';
				/*$rt[$readername['last'] . '-' . $readername['first']] .= '
		<section class="reader-focus">' . apply_filters( 'the_excerpt', $reader->post_excerpt ) . '</section>';*/
				$rt[ $readername['last'] . '-' . $readername['first'] ] .= '</div></div>';
				$rt[ $readername['last'] . '-' . $readername['first'] ] .= '
</article>';
			}

			ksort( $rt );
			$rt = implode( "\n", $rt );

			$rt = sprintf( '%1$s<div class="reader-archive">%2$s</div>', '<p>Place your cursor over the Reader image to view their area of specialty then click on their name to read a full bio.</p>', $rt );

			return $rt;
		}

		/**
		 * Get Registered Vendors
		 *
		 * @access public
		 * @return string the rendered HTML for the list of registered vendors
		 * @since  0.1
		 */
		function get_registered_vendors() {
			$rt      = '';
			$regform = get_post( 202 );
			if ( ! preg_match( '/id="(\d+)"/', $regform->post_content, $matches ) ) {
				return $rt;
			}

			$form         = \GFFormsModel::get_form_meta( $matches[1] );
			$neededfields = array();

			foreach ( $form['fields'] as $field ) {

				if ( empty( $field['defaultValue'] ) && empty( $field['adminLabel'] ) ) {
					continue;
				}

				if ( 'Company Name' == $field['adminLabel'] ) {
					$neededfields['company'] = $field['id'];
				} elseif ( 'Vendor Name' == $field['adminLabel'] ) {
					$neededfields['vendor'] = $field['id'];
				} else if ( '{user:user_email}' == $field['defaultValue'] || 'Vendor Email' == $field['adminLabel'] ) {
					$neededfields['useremail'] = $field['id'];
				} elseif ( 'Participating' == $field['adminLabel'] ) {
					$neededfields['participating'] = $field['id'];
				} else {
					continue;
				}
			}

			$entries = \GFFormsModel::get_leads( $matches[1], 1, 'DESC', '', 0, 1000 );

			$rt = array();

			foreach ( $entries as $entry ) {
				if ( 'Yes, I will be participating' != $entry[ $neededfields['participating'] ] ) {
					continue;
				}

				$user = get_user_by( 'email', $entry[ $neededfields['useremail'] ] );
				if ( ! empty( $user ) && ! is_wp_error( $user ) ) {
					$reader = get_posts( array(
						'author'         => $user->ID,
						'post_type'      => 'vendor',
						'post_status'    => 'publish',
						'posts_per_page' => 1,
					) );
					if ( ! is_array( $reader ) || is_wp_error( $reader ) || empty( $reader ) ) {
						continue;
					}

					$reader  = array_shift( $reader );
					$company = null;
					if ( property_exists( $user, 'data' ) && property_exists( $user->data, 'display_name' ) ) {
						$company = $user->data->display_name;
					}
					if ( ! empty( $company ) ) {
						$readername = array(
							'first' => $company,
							'last'  => '',
						);
						$id         = $readername['first'];
					} else {
						$readername = array(
							'first' => get_user_meta( $user->ID, 'first_name', true ),
							'last'  => get_user_meta( $user->ID, 'last_name', true )
						);
						$id         = $readername['last'] . '-' . $readername['first'];
					}

					if ( has_post_thumbnail( $reader->ID ) ) {
						$avatar_id = get_post_thumbnail_id( $reader->ID );
						list( $avatar ) = wp_get_attachment_image_src( $avatar_id, 'medium', false );
					} else {
						$avatar = get_avatar_url( $user->ID, array( 'size' => 300, 'default' => 'mystery' ) );
					}
				} else {
					$reader     = new \stdClass();
					$reader->ID = 0;

					$avatar = get_avatar_url( 0, array( 'size' => 300, 'default' => 'mystery' ) );

					$company = $entry[ $neededfields['company'] ];
					$vendor  = array(
						'first' => $entry[ $neededfields['vendor'] . '.3' ],
						'last'  => $entry[ $neededfields['vendor'] . '.6' ],
					);

					$id = empty( $company ) ? $vendor['last'] . '-' . $vendor['first'] : $company;
					if ( empty( $company ) ) {
						$readername = array(
							'first' => $vendor['first'],
							'last'  => $vendor['last'],
						);
					} else {
						$readername = array(
							'first' => $company,
							'last'  => '',
						);
					}
				}

				$rt[ $id ] = '
<article class="active-reader">';

				$rt[ $id ] .= sprintf( '<div class="reader-avatar" style="background-image:url(%s)"><div class="reader-info">', $avatar );

				$rt[ $id ] .= '
	<h2 class="reader-name">
		<a href="' . get_permalink( $reader->ID ) . '" title="View the bio of ' . $readername['first'] . ' ' . $readername['last'] . '">
			' . $readername['first'] . ' ' . $readername['last'] . '
		</a>
	</h2>';
				$rt[ $id ] .= '</div></div>';
				$rt[ $id ] .= '
</article>';
			}

			if ( empty( $rt ) ) {
				return '<p><em>There are currently no vendors registered for the upcoming fair. Please check back soon.</em></p>';
			}

			ksort( $rt );

			$rt = implode( "\n", $rt );

			$rt = sprintf( '%1$s<div class="reader-archive">%2$s</div>', '<p>Place your cursor over the Reader image to view their area of specialty then click on their name to read a full bio.</p>', $rt );

			return $rt;
		}

		public function do_fair_date_shortcode( $atts = array() ) {
			if ( array_key_exists( 'format', $atts ) ) {
				$format = $atts['format'];
			} else {
				$format = $this->orders_obj->dateformat;
			}

			return date( $format, $this->orders_obj->fairdate );
		}
	}

}