jQuery( function( $ ) {
	if ( $( 'input.readings-checkbox' ).length > 0 ) {
		if ( window.location.hash == '' || window.location.hash == '#' )
			window.location.hash = $( '.sessionid' ).val();
	}
	$( 'a.nyroModal' ).nyroModal();
	/*$( 'body' ).append( '<div style="display: none" id="nyroHiddenContent" />' );
	$( '.hide-if-js' ).each( function() {
		$( this ).appendTo( '#nyroHiddenContent' ).removeClass( 'hide-if-js' );
	});*/
	$( '.hide-if-js' ).hide();
	$( 'body' ).append( '<div id="dpf-loading-image" style="background: url(/forms/wp-content/plugins/dpf-reservations/jquery.nyroModal/img/ajaxLoader.gif) no-repeat center center; background-color: rgba( 0, 0, 0, .6 ); width: 100%; height: ' + $( window ).height() + 'px; position: fixed; top: 0; left: 0;">&nbsp;</div>' )
	$( '#dpf-loading-image' ).hide();
	
	var lastResults = null;
	
	function update_reservations() {
		clearTimeout( t );

		/*console.log( $( '#reading-selection' ).serialize() );*/

		var data = {
			'action' : dpf_ajax_object.action, 
			'sessionid' : $( '.sessionid' ).val(), 
			'selected' : $( '#reading-selection' ).serialize()
		};
		$.ajax( dpf_ajax_object.ajax_url, {
			'cache' : false, 
			'data' : data, 
			'dataType' : 'json', 
			'error' : function() { 
				t = setTimeout( function() { update_reservations(); }, 5000 ); 
				$( '#dpf-loading-image' ).hide(); 

				/*console.log( 'There was an error retrieving the updated information' );*/

				return;
			}, 
			'success' : function( results ) {
				/* Iterate through all items in the return array and update checkboxes where necessary */
				/*console.log( results );
				console.log( results.messages );*/

				if ( results.hash === lastResults ) {

					/*console.log( 'Skipping update process, because there were no changes since last poll' );*/

					t = setTimeout( function() { update_reservations(); }, 5000 );
					return;
				}
				
				/*console.log( 'There were changes since last poll, so updating the page' );*/

				lastResults = results.hash;
				
				for ( var readerid in results ) {
					for ( var t in results[readerid].times ) {
						var timename = $( 'li[rel="reader-' + readerid + '-' + t + '"]' ).find( 'span[rel="timename"]' ).html();
						if ( 4 === results[readerid].times[t] ) {
							if ( $( 'li[rel="reader-' + readerid + '-' + t + '"]' ).hasClass( 'unavailable' ) ) {
								continue;
							}
							$( 'li[rel="reader-' + readerid + '-' + t + '"]' ).html( '<span rel="timename">' + timename + '</span> - Available at Fair' ).addClass( 'unavailable' ).removeClass( 'reserved' ).removeClass( 'available' );
						} else if ( 3 === results[readerid].times[t] ) {
							if ( $( 'li[rel="reader-' + readerid + '-' + t + '"]' ).hasClass( 'unavailable' ) ) {
								continue;
							}
							$( 'li[rel="reader-' + readerid + '-' + t + '"]' ).html( '<span rel="timename">' + timename + '</span> - On Break' ).addClass( 'unavailable' ).removeClass( 'reserved' ).removeClass( 'available' );
						} else if ( 2 === results[readerid].times[t] ) {
							if ( $( 'li[rel="reader-' + readerid + '-' + t + '"]' ).hasClass( 'reserved' ) ) {
								continue;
							}
							$( 'li[rel="reader-' + readerid + '-' + t + '"]' ).html( '<span rel="timename">' + timename + '</span> - Reserved' ).addClass( 'reserved' ).removeClass( 'unavailable' ).removeClass( 'available' );
						} else if ( 1 === results[readerid].times[t] ) {
							if ( $( 'li[rel="reader-' + readerid + '-' + t + '"]' ).hasClass( 'unavailable' ) ) {
								continue;
							}
							var timename = $( 'li[rel="reader-' + readerid + '-' + t + '"]' ).find( 'span[rel="timename"]' ).html();
							$( 'li[rel="reader-' + readerid + '-' + t + '"]' ).html( '<span rel="timename">' + timename + '</span> - Sold' ).addClass( 'unavailable' ).removeClass( 'reserved' ).removeClass( 'available' );
						} else {
							if ( $( 'li[rel="reader-' + readerid + '-' + t + '"]' ).hasClass( 'available' ) ) {
								continue;
							}
							$( 'li[rel="reader-' + readerid + '-' + t + '"]' ).html( '<input type="checkbox" class="readings-checkbox" name="readings[' + readerid + '][' + t + ']" id="readings_' + readerid + '_' + t + '"/> <label for="readings_' + readerid + '_' + t + '"><span rel="timename">' + timename + '</span> - Available</label>' ).addClass( 'available' ).removeClass( 'unavailable' ).removeClass( 'reserved' );
						}
					}
				}
				$( 'input.readings-checkbox' ).unbind( 'change' );
				$( 'input.readings-checkbox' ).bind( 'change', function() { $( '#dpf-loading-image' ).height( $( window ).height() ).show(); update_reservations(); return true; } );
				$( '#dpf-loading-image' ).hide();
				t = setTimeout( function() { update_reservations(); }, 5000 );
			}
		} );
	}
	
	var t = setTimeout( function() { update_reservations(); }, 5000 );
	
	$( 'input.readings-checkbox' ).change( function() {
		$( '#dpf-loading-image' ).height( $( window ).height() ).show();
		update_reservations();
		return true;
	} );
	$( '#order-form' ).submit( function() {
		if( $( '#reading-selection input[type="checkbox"]:checked' ).length <= 0 ) {
			alert( 'You have not selected any time slots above.' );
			return false;
		}
		
		var selected = [];
		var selectedText = [];
		$( '#reading-selection input[type="checkbox"]:checked' ).each( function() {
			var tmpName = $( this ).attr( 'name' );
			selectedText.push( $( this ).parent().find( 'span[rel="timename"]' ).text() + ' with ' + $.trim( $( this ).closest( '.times' ).parent( 'li' ).find( 'h2.reader-name' ).text() ) );
			var nameRegEx = /^readings\[([0-9]{1,})\]\[([0-9]{1,})\]$/;
			var myregex = new RegExp( nameRegEx );
			if ( myregex.test( tmpName ) ) {
				var IDs = myregex.exec( tmpName );
				/*console.log( IDs );*/
				var readerID = IDs[1];
				var timeID = IDs[2];
				selected.push( '[' + readerID + '][' + timeID + ']' );
			}
		} );
		/*console.log( selected );*/
		$( 'input[name="readings"]' ).val( selected.join( ',' ) );
		$( 'input[name="readings-text"]' ).val( selectedText.join( ' \n' ) );
		$( 'input[name="qty"]' ).val( $( 'input.readings-checkbox:checked' ).length );
		$( 'input[name="total"]' ).val( 15 );
		/*return false;*/
		/*console.log( $( this ).serialize() );*/
		return true;
	} );
} );
