<?php

namespace {
	if ( ! defined( 'ABSPATH' ) ) {
		die( 'You do not have permission to access this file directly.' );
	}
}

namespace DPF {
	class ActiveVendors extends \WP_Widget {
		function __construct() {
			parent::__construct( false, __( 'Active Vendors', 'dpf' ) );
		}

		function widget( $args, $instance ) {
			$title = apply_filters( 'widget_title', $instance['title'] );
			echo $args['before_widget'];
			if ( $title ) {
				echo $args['before_title'] . $title . $args['after_title'];
			}
			echo \DPF\Shortcodes::instance()->get_registered_vendors();
			echo $args['after_widget'];
		}

		function update( $new_instance, $old_instance ): array {
			$instance          = $old_instance;
			$instance['title'] = $new_instance['title'];

			return $instance;
		}

		function form( $instance ) {
			$title = esc_attr( $instance['title'] );
			?>
			<p>
				<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
				<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>"
				       name="<?php echo $this->get_field_name( 'title' ); ?>" type="text"
				       value="<?php echo $title; ?>"/>
			</p>
			<?php
		}
	}
}