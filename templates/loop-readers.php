<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package    WordPress
 * @subpackage Twenty_Twelve
 * @since      Twenty Twelve 1.0
 */
remove_all_actions( 'genesis_loop' );
add_action( 'genesis_loop', 'dpf_orders_reader_loop' );

function dpf_orders_reader_loop() {
	?>
    <p>Place your cursor over the Reader image to view their area of specialty then click on their name to read a full bio.</p>
	<div class="reader-archive">
	<?php
	if ( have_posts() ) : while ( have_posts() ) : the_post();
		?>
		<article class="reader">
			<?php
			if ( has_post_thumbnail() ) {
				$avatar_id = get_post_thumbnail_id();
				$avatar    = wp_get_attachment_image_src( $avatar_id, 'medium', false );
				$avatar    = $avatar[0];
			} else {
				$avatar = get_avatar_url( get_the_author_meta( 'user_email' ), array( 'size'    => 300,
																					  'default' => 'mystery'
				) );
			}
			?>
			<div class="reader-avatar" style="background-image: url(<?php echo $avatar ?>)">
				<div class="reader-info">
					<h2 class="reader-name">
						<a href="<?php the_permalink() ?>" title="<?php the_title_attribute() ?>">
							<?php echo( 'reader' == get_post_type() ? get_the_author() : get_the_title() ) ?>
						</a>
					</h2>
					<section class="focus">
						<?php the_excerpt() ?>
					</section>
				</div>
			</div>
		</article>
		<?php
	endwhile; endif;
	?>
	</div>
	<?php
	
	wp_reset_query();
}