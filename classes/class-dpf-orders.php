<?php
/**
 * Implements the main DPF Orders class
 *
 * @since 0.1a
 * @version 1.0
 */

class DPF_Orders {
	private static $instance;
	public $fairdate = null;
	public $livedate = null;
	public $closedate = null;
	public $prevfair = null;
	public $nextfair = null;
	public $nextopen = null;
	public $currenttime = null;
	/*
	 * @var array $afair the time slots that should only be available for purchase at the fair
	 * These items correspond with the keys of the $timevalues variable
	 */
	public $afair = array( 3, 4, 7, 8, 12, 13, 16, 17, 21, 22, 25, 26 );

	public $timeformat = null;
	public $dateformat = null;

	public $times = array();
	public $alltimes = array();
	public $readers = array();
	/*
	 * @var array $timevalues the full list of available time slots
	 */
	public $timevalues = array(
		1  => 1200,
		2  => 1215,
		3  => 1230,
		4  => 1245,
		5  => 1300,
		6  => 1315,
		7  => 1330,
		8  => 1345,
		9  => 1400,
		11 => 1415,
		12 => 1430,
		13 => 1445,
		14 => 1500,
		15 => 1515,
		16 => 1530,
		17 => 1545,
		18 => 1600,
		19 => 1615,
		21 => 1630,
		22 => 1645,
		23 => 1700,
		24 => 1715,
		25 => 1730,
		26 => 1745,
	);
	public $scriptversion = '2.0.6';

	/**
	 * Returns the instance of this class.
	 *
	 * @access  public
	 * @return  DPF_Orders
	 * @since   2.0
	 */
	public static function instance() {
		if ( ! isset( self::$instance ) ) {
			$className      = __CLASS__;
			self::$instance = new $className;
		}

		return self::$instance;
	}

	/**
	 * Construct our DPF_Orders object
	 */
	function __construct() {
		/* Temporarily make all formerly "Available at Fair" time slots available for pre-order */
		$this->afair = array();

		add_action( 'plugins_loaded', array( $this, 'setup' ) );
		add_action( 'template_redirect', array( $this, 'genesis_tweaks' ) );
	}

	/**
	 * Perform most of the things we need to set up in order to use this
	 *      class. We are not doing this as part of the __construct()
	 *      method, because instantiating our secondary classes during
	 *      __construct() was causing duplication of efforts/resources
	 *
	 * @access public
	 * @return void
	 * @since  2.0
	 */
	function setup() {
		add_filter( 'jetpack_photon_reject_https', '__return_false' );

		$this->get_important_dates();

		global $wpdb;
		$this->fairid = $wpdb->get_var( $wpdb->prepare( "SELECT ID FROM {$wpdb->posts} WHERE post_type=%s AND post_title=%s", 'post', date( "l, F jS, Y", $this->fairdate ) ) );
		if ( empty( $this->fairid ) || is_wp_error( $this->fairid ) ) {
			$this->create_fair_post();
		}

		add_filter( 'gform_pre_render', array( $this, 'dpf_order_form_fields' ) );

		add_filter( 'gform_pre_render_1', array( $this, 'check_form_schedule' ), 10, 1 );
		add_filter( 'gform_pre_process_1', array( $this, 'check_form_schedule' ), 10, 1 );

		add_action( 'genesis_entry_content', array( $this, 'add_reader_focus' ), 9 );
		add_filter( 'the_title', array( $this, 'do_reader_title' ) );
		add_filter( 'single_post_title', array( $this, 'do_reader_title_element' ) );

		add_action( 'wp_ajax_dpf_reserve_spots', array( $this, 'ajax_action' ) );
		add_action( 'wp_ajax_nopriv_dpf_reserve_spots', array( $this, 'ajax_action' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'styles' ) );

		add_action( 'template_redirect', array( $this, 'get_reader_loop' ) );
		add_action( 'template_redirect', array( $this, 'get_vendor_loop' ) );

		if ( ! is_user_logged_in() && ! is_admin() ) {
			add_action( 'wp_head', array( $this, 'analytics' ) );
		}

		add_filter( 'wp_print_footer_scripts', array( $this, 'order_form_script' ), 99 );

		/**
		 * Make sure that any reserved spots are marked as sold in the database
		 */
		add_action( 'gform_after_submission_1', array( $this, 'save_reserved_spots' ), 10, 2 );

		add_action( 'init', array( $this, 'register_post_type' ) );
		add_action( 'admin_init', array( $this, 'user_caps' ) );

		add_filter( 'gform_entry_meta', array( $this, 'reserve_entry_meta' ), 10, 2 );

		add_filter( 'user_contactmethods', array( $this, 'extended_profile_fields' ) );

		add_filter( 'slt_fsp_caps_check', array( $this, 'my_caps_check' ) );

		if ( ! class_exists( '\DPF\Shortcodes' ) ) {
			require_once( plugin_dir_path( __FILE__ ) . '/class-dpf-shortcodes.php' );
			$this->shortcodes = \DPF\Shortcodes::instance();
		}

		if ( ! class_exists( '\DPF\Dashboard' ) ) {
			require_once( plugin_dir_path( __FILE__ ) . '/class-dpf-dashboard.php' );
			$this->dashboard = \DPF\Dashboard::instance();
		}

		if ( ! class_exists( '\DPF\ActiveReaders' ) ) {
		    require_once plugin_dir_path( __FILE__ ) . '/widgets/active-readers.php';
        }

		if ( ! class_exists( '\DPF\ActiveVendors' ) ) {
		    require_once plugin_dir_path( __FILE__ ) . '/widgets/active-vendors.php';
        }

		add_action( 'widgets_init', array( $this, 'register_widgets' ) );

		add_filter( 'wcpv_vendor_slug', array( $this, 'change_wcpv_vendor_slug' ) );
	}

	public function register_widgets() {
		register_widget( '\DPF\ActiveReaders' );
		register_widget( '\DPF\ActiveVendors' );
	}

	public function genesis_tweaks() {
	    if ( ! defined( 'WC_PRODUCT_VENDORS_TAXONOMY' ) ) {
	        return;
        }

	    if ( is_tax( WC_PRODUCT_VENDORS_TAXONOMY ) || ( is_product() ) ) {
	        $name = 'header_image';
            add_filter( "theme_mod_{$name}", array( $this, 'custom_vendor_banner' ) );

		    $term = get_queried_object_id();
        }
    }

    public function custom_vendor_banner( $header ) {
	    if ( is_product() ) {
	        global $post;
		    $terms = wp_get_post_terms( $post->ID, WC_PRODUCT_VENDORS_TAXONOMY );
		    if ( is_array( $terms ) ){
		        $term = array_shift( $terms );
		        $term = $term->term_id;
            }
        } else if ( is_tax( WC_PRODUCT_VENDORS_TAXONOMY ) ) {
		    $term = get_queried_object_id();
	    }

	    if ( empty( $term ) || ! is_numeric( $term ) ) {
	        return $header;
        }

	    $banner = get_field( 'store_banner', WC_PRODUCT_VENDORS_TAXONOMY . '_' . $term );

	    if ( ! empty( $banner ) ) {
	        $header = $banner;
        }

	    return $header;
    }

	function my_caps_check( $caps ) {
		$caps[] = 'update_core';

		return $caps;
	}

	function user_caps() {
		$v = get_role( 'vendor' );
		if ( is_object( $v ) ) {
			$v->add_cap( 'level_1' );
		}
	}

	/**
	 * Add some JavaScript to stop people from submitting their order more than once
	 */
	function order_form_script() {
		if ( ! is_page( 'reservation-form' ) ) {
			return;
		}

		$content = '
<script>jQuery( function( $ ) {
$( \'.gform_footer input[type="submit"]\' ).one( \'click\', function() {
		/*$( this ).attr( \'disabled\', true ).attr( \'value\', \'Loading\' );*/
		$( this ).hide();
		$( this ).after( \'<input type="button" class="button gform_button" value="Loading"/>\' );
		return true;
	} );
} );</script>';

		echo $content;
	}

	/**
	 * Insert the reader's focus before their bio on a single reader display
	 */
	function add_reader_focus() {
		if ( ! is_singular( 'reader' ) ) {
			return;
		}

		global $post;
		if ( ! is_object( $post ) || ! property_exists( $post, 'post_excerpt' ) || empty( $post->post_excerpt ) ) {
			return;
		}

		printf( '<section class="reader-focus">Focus: %s</section>', $post->post_excerpt );
	}

	/**
	 * Use the reader's display name as the title of the page/post
	 *        instead of the post name
	 */
	function do_reader_title( $title = '' ) {
		if ( is_admin() ) {
			return $title;
		}

		if ( ! is_singular( 'reader' ) && ! is_post_type_archive( 'reader' ) ) {
			return $title;
		}

		if ( 'reader' != get_post_type() ) {
			return $title;
		}

		$author = get_the_author();
		if ( empty( $author ) || is_wp_error( $author ) ) {
			return $title;
		}

		return $author;
	}

	/**
	 * User the reader's display name as the HTML title of the page/post
	 *        instead of the post name
	 */
	function do_reader_title_element( $title = '' ) {
		if ( is_admin() ) {
			return $title;
		}

		if ( ! is_singular( 'reader' ) ) {
			return $title;
		}

		if ( ! function_exists( 'get_queried_object' ) ) {
			return $title;
		}

		global $post;
		if ( ! is_object( $post ) || 'reader' != $post->post_type ) {
			return $title;
		}

		$author = get_userdata( intval( $post->post_author ) );

		return $author->display_name;
	}

	/**
	 * Short-circuit the default loop for the reader archive
	 */
	function get_reader_loop() {
		if ( ! is_post_type_archive( 'reader' ) ) {
			return;
		}

		query_posts( array(
			'post_type'      => 'reader',
			'posts_per_page' => '-1',
			'post_status'    => 'publish',
			'orderby'        => 'menu_order name',
			'order'          => 'asc'
		) );

		include_once( plugin_dir_path( dirname( __FILE__ ) ) . '/templates/loop-readers.php' );
	}

	function get_vendor_loop() {
		if ( ! is_post_type_archive( 'vendor' ) ) {
			return;
		}

		query_posts( array(
			'post_type'      => 'vendor',
			'posts_per_page' => '-1',
			'post_status'    => 'publish',
			'orderby'        => 'menu_order name',
			'order'          => 'asc'
		) );

		include_once( plugin_dir_path( dirname( __FILE__ ) ) . '/templates/loop-readers.php' );
	}

	/**
	 * Calculate the important dates for this plugin
	 */
	function get_important_dates() {
		if ( defined( 'FAIR_WEEK_NUMBER' ) && is_numeric( FAIR_WEEK_NUMBER ) && FAIR_WEEK_NUMBER < 5 ) {
			switch ( FAIR_WEEK_NUMBER ) {
				case 1 :
					$sun = 'first';
					break;
				case 2 :
					$sun = 'second';
					break;
				case 3 :
					$sun = 'third';
					break;
				case 4 :
					$sun = 'fourth';
					break;
				default :
					$sun = 'first';
					break;
			}
		} else {
			$sun = 'first';
		}

		$this->fairdate = strtotime( $sun . ' Sunday of this month', current_time( 'timestamp' ) );
		if ( strtotime( '+1 day', $this->fairdate ) <= current_time( 'timestamp' ) ) {
			$this->fairdate = strtotime( $sun . ' Sunday of next month', current_time( 'timestamp' ) );
		}
		$this->prevfair = strtotime( $sun . ' Sunday of last month', $this->fairdate );
		$this->nextfair = strtotime( $sun . ' Sunday of next month', $this->fairdate );

		$this->livedate  = strtotime( '-1 week', $this->fairdate );
		$this->livedate  = strtotime( 'noon', $this->livedate );
		$this->nextopen  = strtotime( '-1 week', $this->nextfair );
		$this->nextopen  = strtotime( 'noon', $this->nextopen );
		$this->closedate = strtotime( '-1 day', $this->fairdate );
		$this->closedate = strtotime( '9 pm', $this->closedate );

		/* Temporarily set the pre-scheduling board to close at the end of the fair, rather than the day before */
		//$this->closedate = strtotime( '6 pm', $this->fairdate );

		$this->timeformat = get_option( 'time_format', 'g:i a' );
		$this->dateformat = get_option( 'date_format', 'l, F jS, Y' );

		/*echo "<!-- \n";
		echo "Fair Date is: " . date( $this->dateformat . " " . $this->timeformat, $this->fairdate ) . "\n";
		echo "Live Date is: " . date( $this->dateformat . " " . $this->timeformat, $this->livedate ) . "\n";
		echo "Close Date is: " . date( $this->dateformat . " " . $this->timeformat, $this->closedate ) . "\n";
		echo "Next Fair Date is: " . date( $this->dateformat . " " . $this->timeformat, $this->nextfair ) . "\n";
		echo "Next Open Date is: " . date( $this->dateformat . " " . $this->timeformat, $this->nextopen ) . "\n";
		echo " -->\n";*/
	}

	/**
	 * Create a post to store information about the upcoming fair
	 */
	function create_fair_post() {
		$fairpost = array(
			'post_type'      => 'post',
			'post_status'    => 'publish',
			'post_author'    => 1,
			'comment_status' => 'closed',
			'ping_status'    => 'closed',
			'post_content'   => '',
			'post_excerpt'   => '',
			'post_title'     => date( "l, F jS, Y", $this->fairdate ),
			'post_category'  => array( 4 ),
		);

		$this->fairid = wp_insert_post( $fairpost );
		update_post_meta( $this->fairid, '_fair_timestamp', $this->fairdate );
	}

	/**
	 * Inject the Google Analytics tracking code into the head
	 */
	function analytics() {
		?>
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-881880-15', 'dallaspsychicfair.com');
            ga('send', 'pageview');

        </script>
		<?php
	}

	/**
	 * Parse the form data and update the database
	 */
	function save_reserved_spots( $entry, $form ) {
		$this->log( 'Entered the save_reserved_spots method' );

		$this->retrieve_times();

		$selected = $entry['7.1'];
		$selected = explode( ',', $selected );
		$this->log( 'Selected time slots are: ' . print_r( $selected, true ) );

		$readings = array();
		foreach ( $selected as $s ) {
			preg_match( '/\[([0-9]+)\]\[([0-9]+)\]/', $s, $matches );
			if ( 3 == count( $matches ) ) {
				$readings[ $matches[1] ][] = $matches[2];
			}
		}

		$this->update_times( $readings );

		return;
	}

	/**
	 * Update database with sold times
	 */
	function update_times( $times ) {
		$this->log( 'Entered the update_times method' );

		if ( ! is_array( $times ) ) {
			$this->log( 'The times var was not an array. It looked like this: ' . print_r( $times, true ) );

			return false;
		}

		global $wpdb;
		foreach ( $times as $userid => $spots ) {
			$reader = $this->readers[ $userid ];
			foreach ( $spots as $spot ) {
				$this->readers[ $userid ]->times[ $spot ] = 1;
				$timekeys                                 = array_flip( $this->timevalues );
				$i                                        = $timekeys[ $spot ];
				$tablename                                = $wpdb->prefix . 'gf_entry_meta';
				$query                                    = $wpdb->prepare( "SELECT id FROM {$tablename} WHERE entry_id=%d AND form_id=%d AND meta_key=%s", $this->readers[ $userid ]->entryID, $this->readers[ $userid ]->formID, $this->regforminfo['soldfield'] . '.' . $i );
				$lead_detail_id                           = $wpdb->get_var( $query );
				$this->log( 'The query to find the reader looks like: ' . $query );
				$this->log( 'The results of that query look like: ' . print_r( $lead_detail_id, true ) );
				if ( empty( $lead_detail_id ) || is_wp_error( $lead_detail_id ) ) {
					/*print( '<pre><code>Item #' . $this->regforminfo['soldfield'] . '.' . $i . ' was not yet listed as sold.' );
					print( "\n" );
					var_dump( array( 'lead_id' => $this->readers[$userid]->entryID, 'form_id' => $this->readers[$userid]->formID, 'field_number' => $this->regforminfo['soldfield'] . '.' . $i, 'value' => $spot ) );
					print( "\n" );
					var_dump( $wpdb->insert( $tablename, array( 'lead_id' => $this->readers[$userid]->entryID, 'form_id' => $this->readers[$userid]->formID, 'field_number' => $this->regforminfo['soldfield'] . '.' . $i, 'value' => $spot ), array( '%d', '%d', '%s', '%d' ) ) );
					print( '</code></pre>' );*/
					$wpdb->insert( $tablename, array(
						'entry_id'   => $this->readers[ $userid ]->entryID,
						'form_id'    => $this->readers[ $userid ]->formID,
						'meta_key'   => $this->regforminfo['soldfield'] . '.' . $i,
						'meta_value' => $spot
					), array( '%d', '%d', '%s', '%d' ) );
					/*} else {
						print( '<pre><code>Item #' . $this->regforminfo['soldfield'] . '.' . $i . ' was already sold.</code></pre>' );*/
				}

				delete_transient( 'openings_' . $userid . '_' . $spot );
			}
		}

		return;
	}

	/**
	 * Retrieve meta information about a sold spot
	 */
	function reserve_entry_meta( $entry_meta, $form_id ) {
		return $entry_meta;
	}

	/**
	 * Output the necessary CSS files
	 */
	function styles() {
		wp_enqueue_style( 'dpf-reservations', plugins_url( '/styles/dpf-reservations.css', dirname( __FILE__ ) ), array(), $this->scriptversion, 'all' );
		wp_enqueue_style( 'nyroModal', plugins_url( '/scripts/jquery.nyroModal/styles/nyroModal.css', dirname( __FILE__ ) ), array(), '0.1', 'screen' );
	}

	/**
	 * @deprecated
	 */
	function dpf_order_form_fields( $form ) {
		/*print( '<pre><code>' );
		var_dump( $form );
		print( '</code></pre>' );*/
		return $form;
	}

	/**
	 * Parse the current reservation status and output JSON
	 */
	function ajax_action() {
		$results = $this->reserve_times();

		$optname = sprintf( 'polling-hits-' . sanitize_title( $this->fairdate ) );
		$hits    = get_option( $optname, 0 );
		$hits ++;
		update_option( $optname, $hits );

		$results['hash'] = base64_encode( json_encode( $results ) );

		$results['timestamp'] = time();
		$results['sessionid'] = $_GET['sessionid'];

		echo json_encode( $results );
		die();
	}

	/**
	 * Retrieve a list of the time slots & their status
	 */
	function retrieve_times() {
		$this->alltimes = array(
			'1200' => '12:00 p.m.',
			'1215' => '12:15 p.m.',
			'1230' => '12:30 p.m.',
			'1245' => '12:45 p.m.',
			'1300' => '1:00 p.m.',
			'1315' => '1:15 p.m.',
			'1330' => '1:30 p.m.',
			'1345' => '1:45 p.m.',
			'1400' => '2:00 p.m.',
			'1415' => '2:15 p.m.',
			'1430' => '2:30 p.m.',
			'1445' => '2:45 p.m.',
			'1500' => '3:00 p.m.',
			'1515' => '3:15 p.m.',
			'1530' => '3:30 p.m.',
			'1545' => '3:45 p.m.',
			'1600' => '4:00 p.m.',
			'1615' => '4:15 p.m.',
			'1630' => '4:30 p.m.',
			'1645' => '4:45 p.m.',
			'1700' => '5:00 p.m.',
			'1715' => '5:15 p.m.',
			'1730' => '5:30 p.m.',
			'1745' => '5:45 p.m.',
		);
		$regform        = get_post( 168 );
		if ( ! preg_match( '/id="(\d+)"/', $regform->post_content, $matches ) ) {
			return array();
		}

		$form         = \GFFormsModel::get_form_meta( $matches[1] );
		$neededfields = array();

		foreach ( $form['fields'] as $field ) {

			if ( empty( $field['defaultValue'] ) && empty( $field['adminLabel'] ) ) {
				continue;
			}

			if ( '{user:user_login}' == $field['defaultValue'] ) {
				$neededfields['userlogin'] = (int) $field['id'];
				/*print( '<pre><code>We set the userlogin value to ' . (int) $field['id'] . '</code></pre>' );*/
			} elseif ( '{user:user_email}' == $field['defaultValue'] ) {
				$neededfields['useremail'] = $field['id'];
				/*print( '<pre><code>We set the useremail value to ' . (int) $field['id'] . '</code></pre>' );*/
			} elseif ( 'Break Times' == $field['adminLabel'] ) {
				$neededfields['breaks'] = $field['id'];
				/*print( '<pre><code>We set the breaks value to ' . (int) $field['id'] . '</code></pre>' );*/
			} elseif ( 'Sold Times' == $field['adminLabel'] ) {
				$neededfields['sold'] = $field['id'];
			} elseif ( 'Reserved Times' == $field['adminLabel'] ) {
				$neededfields['reserved'] = $field['id'];
			} elseif ( 'Participating' == $field['adminLabel'] ) {
				$neededfields['participating'] = $field['id'];
			} else {
				$this->log( 'The field that looks like the following did not match the fields we were searching for' . print_r( $field, true ) );
				continue;
			}
		}

		/*$entries                  = \GFFormsModel::get_leads( $matches[1], 1, 'DESC', '', 0, 1000 );*/
		$entries                  = \GFAPI::get_entries( $matches[1], array(), array(
			'key'        => 1,
			'direction'  => 'DESC',
			'is_numeric' => false
		), array( 'page_size' => 1000 ) );
		$this->registered_readers = $entries;
		$this->regforminfo        = array(
			'soldfield'  => $neededfields['sold'],
			'resfield'   => $neededfields['reserved'],
			'breakfield' => $neededfields['breaks'],
		);

		$rt = array();

		foreach ( $entries as $entry ) {
			/*print( '<pre><code>' );
			var_dump( $entry );
			print( '</code></pre>' );*/
			$is_vendor = false;

			if ( 'Yes, I will' != $entry[ $neededfields['participating'] ] ) {
				continue;
			}

			if ( ! array_key_exists( 'userlogin', $neededfields ) || empty( $neededfields['userlogin'] ) ) {
				continue;
			}

			$user   = get_user_by( 'email', $entry[ $neededfields['useremail'] ] );
			$reader = get_posts( array(
				'author'         => $user->ID,
				'post_type'      => 'reader',
				'post_status'    => 'publish',
				'posts_per_page' => 1,
			) );
			if ( ! is_array( $reader ) || is_wp_error( $reader ) || empty( $reader ) ) {
				$is_vendor = true;
				$reader    = get_posts( array(
					'author'         => $user->ID,
					'post_type'      => 'vendor',
					'post_status'    => 'publish',
					'posts_per_page' => 1,
				) );
			}

			if ( ! is_array( $reader ) || is_wp_error( $reader ) || empty( $reader ) ) {
				continue;
			}

			$reader = array_shift( $reader );
			if ( ! is_object( $reader ) ) {
				continue;
			}

			if ( ! $is_vendor ) {
				$readername = array(
					'first' => get_user_meta( $user->ID, 'first_name', true ),
					'last'  => get_user_meta( $user->ID, 'last_name', true )
				);
			} else {
				$readername = array(
					'first' => $user->display_name,
					'last'  => ''
				);
			}

			if ( empty( $readername['first'] ) && empty( $readername['last'] ) ) {
				continue;
			}

			$this->readers[ $user->ID ]               = clone $reader;
			$this->readers[ $user->ID ]->user_account = clone $user;
			$this->readers[ $user->ID ]->entryID      = $entry['id'];
			$this->readers[ $user->ID ]->formID       = $entry['form_id'];
			if ( $is_vendor ) {
				$this->readers[ $user->ID ]->is_vendor = true;
			}

			$summary = '
<article class="reader-summary" id="reader-' . $reader->ID . '">';
			if ( has_post_thumbnail( $reader->ID ) ) {
				$avatar_id = get_post_thumbnail_id( $reader->ID );
				list( $avatar ) = wp_get_attachment_image_src( $avatar_id, 'medium', false );
			} else {
				$avatar = get_avatar_url( $user->ID, array( 'size' => 300, 'default' => 'mystery' ) );
			}
			$summary .= '
	<div class="reader-avatar" style="background-image: url(' . $avatar . ')">
		<div class="reader-info">';
			$summary .= '
			<h2 class="reader-name">
				<a href="#reader-bio-' . $reader->ID . '" class="nyroModal">
					' . get_user_meta( $user->ID, 'first_name', true ) . ' ' . get_user_meta( $user->ID, 'last_name', true ) . '
				</a>
			</h2>';
			$summary .= '
			<p class="reader-focus">
				' . $reader->post_excerpt . '
			</p>';
			$summary .= '
			<article id="reader-bio-' . $reader->ID . '" class="hide-if-js">
				' . apply_filters( 'the_content', $reader->post_content ) . '
			</article>';
			$summary .= '
		</div>
	</div>';

			$summary .= '
</article>';

			$this->readers[ $user->ID ]->summary = $summary;
			$this->readers[ $user->ID ]->times   = array();

			$timekeys = array_flip( $this->timevalues );

			foreach ( $this->alltimes as $t => $l ) {
				$i = $timekeys[ $t ];

				$reserved = get_transient( 'openings_' . $user->ID . '_' . $t );
				if ( ! empty( $reserved ) ) {
					$entry[ $neededfields['reserved'] . '.' . $i ] = $t;
				}

				if ( array_key_exists( $neededfields['sold'] . '.' . $i, $entry ) && ! empty( $entry[ $neededfields['sold'] . '.' . $i ] ) ) {
					$this->readers[ $user->ID ]->times[ $t ] = 1;
				} elseif ( array_key_exists( $neededfields['breaks'] . '.' . $i, $entry ) && ! empty( $entry[ $neededfields['breaks'] . '.' . $i ] ) ) {
					$this->readers[ $user->ID ]->times[ $t ] = 3;
				} elseif ( array_key_exists( $neededfields['reserved'] . '.' . $i, $entry ) && ! empty( $entry[ $neededfields['reserved'] . '.' . $i ] ) ) {
					$this->readers[ $user->ID ]->times[ $t ] = 2;
				} elseif ( in_array( $i, $this->afair ) ) {
					$this->readers[ $user->ID ]->times[ $t ] = 4;
				} else {
					$this->readers[ $user->ID ]->times[ $t ] = 0;
				}
			}
		}

		return $this->readers;
	}

	/**
	 * Determine the proper status for each time slot
	 */
	function reserve_times() {
		if ( isset( $_GET['sessionid'] ) ) {
			$sessionid = $_GET['sessionid'];
		} else {
			$sessionid = null;
		}

		if ( isset( $_GET['selected'] ) ) {
			parse_str( urldecode( $_GET['selected'] ) );
			if ( ! empty( $readings ) && is_array( $readings ) ) {
				$selected = $readings;
			} else {
				$selected = array();
			}
		} else {
			$selected = array();
		}

		$readers = $this->retrieve_times();

		$times             = array();
		$times['messages'] = '';
		foreach ( $readers as $readerid => $reader ) {
			$times[ $readerid ] = array();
			foreach ( $reader->times as $k => $v ) {
				$times[ $readerid ]['times'][ $k ] = $v;
				if ( in_array( $v, array( 1, 3 ) ) ) {
					continue;
				}

				$taken = get_transient( 'openings_' . $readerid . '_' . $k );
				if ( ! empty( $taken ) && $sessionid != $taken ) {
					$times[ $readerid ]['times'][ $k ] = 2;
				}

				if ( array_key_exists( $readerid, $selected ) && array_key_exists( $k, $selected[ $readerid ] ) ) {
					delete_transient( 'openings_' . $readerid . '_' . $k );
					set_transient( 'openings_' . $readerid . '_' . $k, $sessionid, ( 60 * 15 ) );
					$times[ $readerid ]['times'][ $k ] = 5;
				}
			}

		}

		$times['messages'] = print_r( $selected, true );

		return $times;
	}

	/**
	 * Register a new post type to handle member bios
	 */
	function register_post_type() {
		if ( defined( 'DPF_READER_NAME_S' ) ) {
			$l = array( 's' => DPF_READER_NAME_S, 'p' => DPF_READER_NAME_P );
		} else {
			$l = array( 's' => __( 'Reader' ), 'p' => __( 'Readers' ) );
		}
		$labels = array(
			'name'               => $l['p'],
			'singular_name'      => $l['s'],
			'add_new'            => 'Add New',
			'add_new_item'       => sprintf( 'Add New %s', $l['s'] ),
			'edit_item'          => sprintf( 'Edit %s', $l['s'] ),
			'new_item'           => sprintf( 'New %s', $l['s'] ),
			'all_items'          => sprintf( 'All %s', $l['p'] ),
			'view_item'          => sprintf( 'View %s', $l['s'] ),
			'search_items'       => sprintf( 'Search %s', $l['p'] ),
			'not_found'          => sprintf( 'No %s found', strtolower( $l['p'] ) ),
			'not_found_in_trash' => sprintf( 'No %s found in Trash', strtolower( $l['p'] ) ),
			'parent_item_colon'  => '',
			'menu_name'          => $l['p']
		);

		$args = array(
			'labels'             => $labels,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'reader' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => true,
			'menu_position'      => null,
			'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'page-attributes' )
		);

		register_post_type( 'reader', $args );
		add_image_size( 'reader-small', 100, 100, true );
		add_image_size( 'reader-photo', 200, 200, true );

		if ( defined( 'DPF_VENDOR_NAME_S' ) ) {
			$l = array( 's' => DPF_VENDOR_NAME_S, 'p' => DPF_VENDOR_NAME_P );
		} else {
			$l = array( 's' => __( 'Vendor' ), 'p' => __( 'Vendors' ) );
		}
		$labels = array(
			'name'               => $l['p'],
			'singular_name'      => $l['s'],
			'add_new'            => 'Add New',
			'add_new_item'       => sprintf( 'Add New %s', $l['s'] ),
			'edit_item'          => sprintf( 'Edit %s', $l['s'] ),
			'new_item'           => sprintf( 'New %s', $l['s'] ),
			'all_items'          => sprintf( 'All %s', $l['p'] ),
			'view_item'          => sprintf( 'View %s', $l['s'] ),
			'search_items'       => sprintf( 'Search %s', $l['p'] ),
			'not_found'          => sprintf( 'No %s found', strtolower( $l['p'] ) ),
			'not_found_in_trash' => sprintf( 'No %s found in Trash', strtolower( $l['p'] ) ),
			'parent_item_colon'  => '',
			'menu_name'          => $l['p']
		);

		$args = array(
			'labels'             => $labels,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'vendor' ),
			/*'capability_type' => 'vend', 
			'map_meta_cap' => true, */
			'has_archive'        => true,
			'hierarchical'       => true,
			'menu_position'      => null,
			'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'page-attributes' )
		);

		register_post_type( 'vendor', $args );
	}


	function extended_profile_fields( $methods = array() ) {
		$methods['phone'] = __( 'Telephone' );

		return $methods;
	}

	/**
	 * Dynamically modify the gravityform schedule
	 */
	public function check_form_schedule( $form, $ajax = false, $field_values = array() ) {
		$shortcode_obj = \DPF\Shortcodes::instance();

		if ( is_user_logged_in() && current_user_can( 'delete_users' ) ) {
			return $form;
		}

		$this->log( 'The form object currently looks like: ' );
		$this->log( print_r( $form, true ) );

		$schedule = array(
			'scheduleForm'           => true,
			'scheduleStart'          => date( 'm/d/Y', $this->livedate ),
			'scheduleStartHour'      => date( 'g', $this->livedate ),
			'scheduleStartMinute'    => intval( date( 'i', $this->livedate ) ),
			'scheduleStartAmpm'      => date( 'a', $this->livedate ),
			'scheduleEnd'            => date( 'm/d/Y', $this->closedate ),
			'scheduleEndHour'        => date( 'g', $this->closedate ),
			'scheduleEndMinute'      => intval( date( 'i', $this->closedate ) ),
			'scheduleEndAmpm'        => date( 'a', $this->closedate ),
			'schedulePendingMessage' => apply_filters( 'dpf-not-open-yet', sprintf( __( '<p><em>This order form is currently closed. It will open at <strong>%s on %s</strong>. Thank you.</em></p>' ), date( $this->timeformat, $this->livedate ), date( $this->dateformat, $this->livedate ) ) ),
			'scheduleMessage'        => apply_filters( 'dpf-not-open-yet', sprintf( __( '<p><em>This order form has closed until the next fair. You will be able to reserve spots for our next fair at <strong>%s on %s</strong>. Thank you.</em></p>' ), date( $this->timeformat, $this->nextopen ), date( $this->dateformat, $this->nextopen ) ) ),
		);

		$form = array_merge( $form, $schedule );

		return $form;
	}

	function log( $message = '' ) {
		if ( ! defined( 'WP_DEBUG' ) || true !== WP_DEBUG ) {
			return;
		}

		error_log( '[DPF Debug]: ' . $message );
	}

	/**
	 * Modify the slug used for WC Product Vendors terms, so it doesn't clash with the existing DPF vendor slug
	 *
	 * @param $slug the existing slug
	 *
	 * @access public
	 * @return string the updated slug
	 * @since  2020.03.29
	 */
	public function change_wcpv_vendor_slug( $slug ) {
		return 'wc-vendor';
	}
}