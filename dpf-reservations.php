<?php
/*
Plugin Name: DPF Reservations
Description: Implements the reservation form for DPF
Version: 2.0
Author: Curtiss Grymala
License: GPL2
*/

if ( ! class_exists( 'DPF_Orders' ) ) {
	require_once( plugin_dir_path( __FILE__ ) . '/classes/class-dpf-orders.php' );
}

/**
 * Instantiate the main DPF_Orders object
 *
 * @access public
 * @since  0.1
 * @return void
 */
function inst_dpf_orders_obj() {
	global $dpf_orders_obj;
	$dpf_orders_obj = DPF_Orders::instance();
}

inst_dpf_orders_obj();